import React, {useCallback} from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import color from '../utils/color';
import {useSelector} from 'react-redux';
import {fonts} from '../utils/fonts';

export default function HeaderPopUp(props) {
  const user = useSelector(state => state.user);

  const renderOperator = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>Daftar Kegiatan</Text>
        <Text style={styles.txtContent}>Validasi 20 Berkas Pemohon</Text>
        <Text style={styles.txtContent}>Mengembalikan 2 Berkas Pemohon</Text>
      </View>
    );
  }, []);

  const renderVerifikator = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>Daftar Kegiatan</Text>
        <Text style={styles.txtContent}>Verifikasi 20 Berkas Pemohon</Text>
        <Text style={styles.txtContent}>Mengembalikan 2 Berkas Pemohon</Text>
      </View>
    );
  }, []);
  const renderAdminDinas = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Jangan Lupa Selesaikan Tugasmu
        </Text>
        <Text style={styles.txtContent}>Terdapat 2 Perizinan Yang Masuk</Text>
        <Text style={styles.txtContent}>
          Terdapat 3 Hasil Survey Yang Masuk
        </Text>
      </View>
    );
  }, []);

  const renderSurveyor = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>Daftar Kegiatan</Text>
        <Text style={styles.txtContent}>Verifikasi 20 Berkas Pemohon</Text>
        <Text style={styles.txtContent}>Mengembalikan 2 Berkas Pemohon</Text>
      </View>
    );
  }, []);

  const renderKepalaDinas = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Kamu Dapat Melihat Perizinan dan Ulasan Pelayanan
        </Text>
        <Text style={styles.txtContent}>Terdapat 23 Jumlah Perizinan</Text>
        <Text style={styles.txtContent}>Terdapat 3 Jumlah Ulasan</Text>
      </View>
    );
  }, []);
  const renderWalikota = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Kamu Dapat Melihat Perizinan dan Ulasan Pelayanan
        </Text>
        <Text style={styles.txtContent}>Terdapat 23 Jumlah Perizinan</Text>
        <Text style={styles.txtContent}>Terdapat 3 Jumlah Ulasan</Text>
      </View>
    );
  }, []);

  const renderAudit = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Kamu Dapat Melihat Perizinan dan Ulasan Pelayanan
        </Text>
        <Text style={styles.txtContent}>Perizinan Masuk (23)</Text>
        <Text style={styles.txtContent}>Perizinan Diproses (23)</Text>
        <Text style={styles.txtContent}>Perizinan Terlambat (23)</Text>
        <Text style={[styles.txtContent, {marginTop: 10}]}>
          Terdapat 3 Jumlah Ulasan
        </Text>
      </View>
    );
  }, []);

  const renderAdminUtama = useCallback(() => {
    return (
      <View style={styles.containerTitle}>
        <Text style={styles.txtTitle}>
          Halo Asep, Kamu Dapat Melihat Perizinan dan Ulasan Pelayanan
        </Text>
        <Text style={styles.txtContent}>Terdapat 23 Jumlah Perizinan</Text>
        <Text style={styles.txtContent}>Terdapat 3 Jumlah Ulasan</Text>
      </View>
    );
  }, []);

  return (
    <>
      <View style={styles.container}>
        <View style={styles.containerImg}>
          <Image
            source={require('../assets/logo_beranda_popup.png')}
            style={styles.img}
            resizeMode="cover"
          />
        </View>
        {user.role === 'Operator' && renderOperator()}
        {user.role === 'Verifikator' && renderVerifikator()}
        {user.role === 'AdminDinas' && renderAdminDinas()}
        {user.role === 'Surveyor' && renderSurveyor()}
        {user.role === 'KepalaDinas' && renderKepalaDinas()}
        {user.role === 'Bupati' && renderWalikota()}
        {user.role === 'Auditor' && renderAudit()}
        {user.role === 'AdminUtama' && renderAdminUtama()}
      </View>
      <View style={{height: 45}} />
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: color.white,
    borderRadius: 20,
    marginHorizontal: 27,
    paddingHorizontal: 19,
    paddingVertical: 22,
    zIndex: 9999,
    alignItems: 'center',
    position: 'absolute',
    top: -75,
    elevation: 5,
  },
  containerImg: {
    width: 78,
    height: 76,
  },
  containerTitle: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 18,
  },
  containerButton: {
    flex: 1,
    paddingTop: 12,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  txtTitle: {
    fontSize: 18,
    color: color.black,
    fontFamily: fonts.montserratBold,
    fontWeight: '700',
    marginBottom: 6,
  },
  txtContent: {
    fontSize: 13,
    color: 'rgba(32, 32, 32, 1)',
    fontFamily: fonts.montserratReguler,
  },
  txtButton: {
    color: color.primary,
    fontFamily: fonts.montserratReguler,
    fontSize: 14,
  },
});
