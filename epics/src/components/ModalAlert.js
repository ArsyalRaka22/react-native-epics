import React, {Component} from 'react';
import {View, Text, Modal, TouchableOpacity, Image} from 'react-native';
import color from '../utils/color';

export default function ModalAlert(props) {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={props.visible}
      onRequestClose={() => {
        props.onRequestClose();
      }}>
      <View style={styles.modalRoot}>
        <TouchableOpacity
          activeOpacity={1}
          style={[styles.modalContent, props.contentStyle]}>
          <View style={{width: 140, height: 136, alignSelf: 'center'}}>
            <Image
              source={require('../assets/ilustrasi_modal.png')}
              style={{height: '100%', width: '100%'}}
              resizeMode="cover"
            />
          </View>
          {props.children}
        </TouchableOpacity>
      </View>
    </Modal>
  );
}

const styles = {
  modalRoot: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContent: {
    width: 345,
    padding: 16,
    borderRadius: 10,
    backgroundColor: '#499DB1',
  },
};
