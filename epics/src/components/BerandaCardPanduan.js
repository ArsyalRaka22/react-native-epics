import React from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Button from './Button';
import color from '../utils/color';
import {fonts} from '../utils/fonts';

export default function BerandaCardPanduan(props) {
  return (
    <SafeAreaView>
      <ImageBackground
        source={require('../assets/background_tutorial.png')}
        style={styles.imgCard}>
        <View style={styles.containerCard}>
          <View style={{flexDirection: 'column', flex: 1}}>
            <Text style={styles.txtCard}>{props.title}</Text>
            <Button
              onPress={() => props.onClick()}
              theme={'black'}
              style={{marginTop: 17}}>
              {props.titleButton}
            </Button>
          </View>
          <View>
            <Image
              source={require('../assets/mail.png')}
              style={{height: 100, width: 100}}
            />
          </View>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  imgCard: {
    width: '100%',
    borderRadius: 20,
    overflow: 'hidden',
  },
  containerCard: {
    flexDirection: 'row',
    paddingVertical: 28,
    paddingHorizontal: 22,
  },
  txtCard: {
    fontSize: 18,
    color: color.white,
    fontFamily: fonts.popinsBold,
  },
});
