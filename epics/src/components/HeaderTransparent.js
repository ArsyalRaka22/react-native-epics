import React from 'react';
import {Image, StatusBar, StyleSheet, View} from 'react-native';

export default function HeaderTransparent(props) {
  return (
    <View style={{height: 162}}>
      <View
        style={{
          height: 176,
          width: '100%',
        }}>
        <Image
          source={require('../assets/satu_header.png')}
          style={styles.img}
        />
      </View>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle={'dark-content'}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    height: '100%',
    width: '100%',
  },
});
