import React from 'react';
import {Image, ImageBackground, StyleSheet, View} from 'react-native';
import color from '../utils/color';

export default function HeaderBeranda(props) {
  return (
    <View style={styles.container}>
      <View style={{backgroundColor: color.primary}}>
        <View style={styles.containerLogo}>
          {/* <Image
            source={require('../assets/logo.png')}
            style={styles.logo}
            resizeMode="cover"
          /> */}
        </View>
        {/* <ImageBackground
          source={require('../assets/dashboard.png')}
          style={styles.img}
          resizeMode="cover"
        /> */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {marginBottom: 37},
  img: {
    width: 430,
    height: 154,
  },
  containerLogo: {
    position: 'absolute',
    zIndex: 999,
    top: 14,
    left: 23,
  },
  logo: {
    width: 148,
    height: 73,
  },
});
