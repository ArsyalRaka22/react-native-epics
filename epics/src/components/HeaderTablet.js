import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Image,
  ImageBackground,
} from 'react-native';
import color from '../utils/color';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default function HeaderTablet(props) {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user);

  const isFocused = useIsFocused();
  const [isCount, setCount] = useState(0);

  const [isLoading, setIsLoading] = useState(false);
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const [showPinInputModal, setShowPinInputModal] = useState(false);
  const [value, setValue] = useState('');

  return (
    <>
      <SafeAreaView style={{backgroundColor: color.primary}} />
      <ImageBackground source={require('../assets/background_beranda.png')}>
        <View style={{height: SCREEN_HEIGHT / 4}}>
          <View style={styles.container}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={props.iconProfile}
              style={[styles.containerProfile, {overflow: 'hidden'}]}>
              <Image
                source={require('../assets/dumy_foto.png')}
                style={{height: '100%', width: '100%', borderRadius: 50}}
              />
              {/* <Ionicons name="person-outline" size={24} color={color.black} /> */}
            </TouchableOpacity>
            <View style={styles.containerText}>
              <Text {...props} numberOfLines={1} style={props.textProfile}>
                Nama Lengkap
              </Text>
              <Text {...props} style={props.textAlamat}>
                Hari baru, semangat baru!
              </Text>
            </View>
            <TouchableOpacity
              style={styles.menuRight}
              onPress={props.iconRight}
              activeOpacity={0.8}>
              <Image
                source={require('../assets/logo/notification.png')}
                style={{height: 24, width: 24, alignSelf: 'center'}}
                resizeMode="cover"
              />
              {/* <Ionicons name="notifications" size={12} color={color.white} /> */}
              {/* <View
                style={{
                  // backgroundColor: isCount > 1 ? color.danger : color.primary,
                  borderRadius: 20,
                  width: 10,
                  height: 10,
                  //   position: 'absolute',
                  top: 5,
                  right: 6,
                }}
              /> */}
            </TouchableOpacity>
          </View>
          {/* <Image
          source={require('../assets/sipena/p.png')}
          style={{
            height: '100%',
            width: '100%',
            position: 'absolute',
            tintColor: color.whiteColorRgba,
          }}
          resizeMode="cover" */}
          {/* /> */}
        </View>
      </ImageBackground>
    </>
  );
}

const styles = {
  container: {
    zIndex: 20,
    flexDirection: 'row',
    alignItems: 'center',
    width: SCREEN_WIDTH,
    paddingVertical: 16,
    paddingHorizontal: 20,
  },
  containerProfile: {
    borderWidth: 1,
    borderColor: color.gray,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.white,
    width: 50,
    height: 50,
    borderRadius: 50,
  },

  containerText: {
    flex: 1,
    paddingHorizontal: 10,
  },

  menuRight: {
    width: 38,
    height: 38,
    borderRadius: 12,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#224B7A',
  },
};
