import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import color from '../utils/color';
import {fonts} from '../utils/fonts';
import Ionicons from 'react-native-vector-icons/Ionicons';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default function ButtonCard(props) {
  return (
    <View style={{flex: 1}}>
      <View style={{flexDirection: 'row'}}>
        {props.labelStatus}
        <View style={{width: 16}} />
        <TouchableOpacity
          onPress={() => {
            props.onPress();
          }}
          style={[
            styles.cardStatus,
            {
              backgroundColor: color.primary,
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 15,
              paddingVertical: 8,
            },
          ]}>
          <Text
            style={{
              color: color.white,
              fontFamily: fonts.popinsMedium,
              fontSize: 15,
            }}>
            Detail Surat
          </Text>
          <View style={{width: 30}} />
          <View
            style={{
              backgroundColor: color.rgbaPrimary,
              paddingLeft: 8,
              paddingRight: 6,
              paddingVertical: 8,
              borderRadius: 5,
            }}>
            <Ionicons
              name="chevron-forward-outline"
              color={color.white}
              size={12}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginHorizontal: 23,
  },
  itemContent: {
    marginTop: 18,
  },
  txtLabeltitle: {
    color: color.black,
    fontSize: 18,
    fontFamily: fonts.popinsBold,
  },
  txtColorPrimary: {
    color: color.primary,
    fontSize: 14,
    fontFamily: fonts.popinsBold,
  },
  mainContent: {
    flex: 1,
    paddingTop: 30,
  },
  childContent: {
    flexDirection: 'column',
    paddingVertical: 25,
  },
  gapFlex: {
    flex: 1,
  },
  rowContent: {
    flexDirection: 'row',
  },
  containerMain: {
    paddingHorizontal: 23,
    paddingBottom: 23,
    paddingTop: 13,
    marginBottom: 36,
  },
  containerTopLabel: {
    paddingTop: 26,
    paddingLeft: 30,
    marginBottom: 13,
  },
  txtSuccess: {
    color: color.labelGreen,
  },
  txtLabelBlack: {
    fontSize: 15,
    fontFamily: fonts.popinsBold,
    color: color.black,
  },
  txtLabelPrimary: {
    fontSize: 15,
    fontWeight: '600',
    color: color.primary,
  },

  txtLabelGreen: {
    color: color.labelGreen,
  },
  txtLabelRed: {
    color: color.labelRed,
  },
  txtLabelYellow: {
    color: color.labelWarning,
  },
  txtLabelTop: {
    fontSize: 15,
    fontWeight: '600',
    color: color.black,
  },
  img: {
    width: 430,
    height: 154,
  },
  txtContent: {
    fontSize: 15,
    fontFamily: fonts.popinsLight,
  },
  txtButtonCard: {
    fontSize: 14,
    fontFamily: fonts.popinsBold,
  },
  contentCardBottom: {
    marginTop: 28,
    paddingLeft: 30,
    flexDirection: 'row',
  },
  contentBtnCard: {
    paddingHorizontal: 40,
    height: 35,
    borderRadius: 4,
  },
  txtProfile: {
    fontFamily: fonts.interBold,
    color: color.white,
    fontSize: 18,
  },
  txtProfileContent: {
    fontFamily: fonts.interLight,
    color: 'rgba(255, 255, 255, 0.70)',
    fontSize: 16,
  },
  menuDashboard: {
    marginHorizontal: 20,
    backgroundColor: color.white,
    paddingVertical: 12,
    paddingHorizontal: 10,
    marginTop: -90,
    // top: 90,
    // position: "absolute",
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderRadius: 12,
  },

  menuChild: {
    width: SCREEN_WIDTH / 4,
    justifyContent: 'center',
    alignContent: 'space-between',
    alignItems: 'center',
    marginVertical: 12,
  },
  containerCard: {
    flexDirection: 'row',
    paddingLeft: 29,
    paddingRight: 42,
    paddingTop: 28,
  },
  txtCard: {
    fontSize: 18,
    color: color.white,
    fontFamily: fonts.popinsBold,
  },
  imgCard: {
    width: '100%',
    height: 174,
    borderRadius: 20,
    overflow: 'hidden',
  },
  txtTitle: {
    fontFamily: fonts.popinsBold,
    color: color.black,
    fontSize: 18,
  },
  cardStatus: {
    flex: 1,
    borderRadius: 10,
    // alignSelf: 'center',
    // alignContent: 'center',
  },
  cardLabel: {
    fontFamily: fonts.popinsMedium,
    paddingVertical: 11,
    textAlign: 'center',
    fontSize: 15,
  },
  cardLabelBtn: {
    fontFamily: fonts.popinsMedium,
    paddingVertical: 11,
    paddingHorizontal: 2,
    fontSize: 15,
  },
  btnForward: {
    backgroundColor: color.rgbaPrimary,
    paddingLeft: 9,
    paddingRight: 7,
    paddingVertical: 8,
    borderRadius: 10,
    marginLeft: 10,
  },
  txtTitleCard: {
    color: '#808080',
    fontSize: 15,
    fontFamily: fonts.popinsLight,
  },
  txtContentCard: {
    fontFamily: fonts.popinsLight,
    color: color.black,
    fontSize: 15,
  },
});
