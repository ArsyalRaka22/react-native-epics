import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {View} from 'react-native';
import color from '../utils/color';

const CheckBox = props => {
  let {box, color, value} = props;
  return (
    <>
      {value && (
        <>
          <View style={[styles.boxCheck, box]}>
            <MaterialCommunityIcons name="check" size={14} color={color} />
          </View>
        </>
      )}
    </>
  );
};

const RadioCheckBox = props => {
  let {box, valueRadio, style} = props;
  return (
    <View style={[styles.box, box]}>
      {valueRadio && (
        <View
          style={[
            {
              width: 10,
              height: 10,
              backgroundColor: color.primary,
              borderRadius: 5,
            },
            style,
          ]}
        />
      )}
    </View>
  );
};

export {RadioCheckBox, CheckBox};

const styles = {
  box: {
    borderWidth: 1,
    borderColor: color.primary,
    borderRadius: 10,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
  },

  boxCheck: {
    borderWidth: 1,
    borderColor: color.primary,
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
    borderRadius: 8,
  },
};
