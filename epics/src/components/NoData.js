import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import color from '../utils/color';
import {fonts} from '../utils/fonts';

export default function NoData(props) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImg}>
        <Image
          source={require('../assets/not_found.png')}
          style={styles.img}
          resizeMode="cover"
        />
      </View>
      <Text style={styles.label}>{props.label}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: color.white,
    paddingVertical: 44,
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  containerImg: {
    width: 159,
    height: 128,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  label: {
    marginTop: 19,
    fontSize: 18,
    fontFamily: fonts.popinsMedium,
    color: color.black,
  },
});
