import React, {useCallback} from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import color from '../utils/color';
import ButtonCard from './ButtonCard';
import {fonts} from '../utils/fonts';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default function CardList(props) {
  const item = props.data;
  const status = props.status;
  const user = useSelector(state => state.user);
  const isJenis = props.jenisMenu ?? 'Pemohon';

  const renderButtonStatus = useCallback(
    status => {
      if (status === 1) {
        return (
          <View
            style={[
              styles.cardStatus,
              {backgroundColor: color.rgbaWarning, alignItems: 'center'},
            ]}>
            <Text style={[styles.cardLabel, {color: color.labelWarning}]}>
              {/* {isJenis === 'Pemohon' && (
                <>
                  {user.role === 'Pemohon' && 'Verifikasi Operator'}
                  {user.role === 'Operator' && 'Dikembalikan'}
                </>
              )} */}
              {user.role === 'Verifikator' && (
                <>
                  {isJenis === 'Perizinan' && 'Verifikasi Verifikator'}
                  {isJenis === 'Survey' && 'Verifikasi Survey'}
                </>
              )}
              {user.role === 'Pemohon' && (
                <>
                  {isJenis === 'Perizinan' && 'Verifikasi Operator'}
                  {/* {isJenis === 'Survey' && 'Verifikasi Survey'} */}
                </>
              )}
              {user.role === 'Operator' && 'Dikembalikan'}
              {user.role === 'AdminDinas' && 'Dikembalikan'}
              {user.role === 'AdminUtama' && 'Dikembalikan'}
              {user.role === 'Auditor' && 'Dikembalikan'}
              {user.role === 'Bupati' && 'Dikembalikan'}
              {user.role === 'Surveyor' && 'Dikembalikan'}
            </Text>
          </View>
        );
      }
      if (status === 2) {
        return (
          <View
            style={[
              styles.cardStatus,
              {
                backgroundColor: color.rgbaGreen,
                alignItems: 'center',
              },
            ]}>
            <Text style={[styles.cardLabel, {color: color.greenColor}]}>
              {user.role === 'Pemohon' && 'Selesai'}
              {user.role === 'Operator' && 'Masuk'}
              {user.role === 'Verifikator' && (
                <>{isJenis === 'Arsip' && 'Selesai'}</>
              )}
              {user.role === 'AdminDinas' && 'Masuk'}
              {user.role === 'AdminUtama' && 'Masuk'}
              {user.role === 'Auditor' && 'Masuk'}
              {user.role === 'Bupati' && 'Masuk'}
              {user.role === 'Surveyor' && 'Masuk'}
            </Text>
          </View>
        );
      }
      if (status === 3) {
        return (
          <View
            style={[
              styles.cardStatus,
              {
                backgroundColor: color.rgbaRed,
                alignItems: 'center',
              },
            ]}>
            <Text style={[styles.cardLabel, {color: color.labelRed}]}>
              {user.role === 'Pemohon' && 'Dikembalikan'}
              {user.role === 'Operator' && 'Terlambat'}
              {user.role === 'AdminDinas' && 'Terlambat'}
              {user.role === 'AdminUtama' && 'Terlambat'}
              {user.role === 'Auditor' && 'Terlambat'}
              {user.role === 'Bupati' && 'Terlambat'}
              {user.role === 'Surveyor' && 'Terlambat'}
            </Text>
          </View>
        );
      }
      if (status === 4) {
        return (
          <View
            style={[
              styles.cardStatus,
              {
                backgroundColor: color.rgbaGray,
                alignItems: 'center',
              },
            ]}>
            <Text style={[styles.cardLabel, {color: color.rgbaMainGray}]}>
              Diterbitkan
            </Text>
          </View>
        );
      }
    },
    [isJenis, user.role],
  );

  return (
    <>
      <View
        style={{
          backgroundColor: '#F0FCFF',
          paddingVertical: 20,
          overflow: 'hidden',
          paddingLeft: 21,
          paddingRight: 15,
        }}>
        <View style={styles.containerCardContent}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
            }}>
            <Text style={styles.txtTitleCard}>Jenis Perizinan</Text>
            <Text style={styles.txtContentCard}>Izin Operasionl</Text>
          </View>
          <View style={{flexDirection: 'column', flex: 1}}>
            <Text style={styles.txtTitleCard}>Nama Instansi</Text>
            <Text style={styles.txtContentCard}>SDN 426 Surabaya</Text>
          </View>
        </View>
        <View style={styles.containerCardContent}>
          <View style={{flexDirection: 'column', flex: 1}}>
            <Text style={styles.txtTitleCard}>Nomor Surat</Text>
            <Text style={styles.txtContentCard}>0113/2023/IO</Text>
          </View>
          <View style={{flexDirection: 'column', flex: 1}}>
            <Text style={styles.txtTitleCard}>Tanggal</Text>
            <Text style={styles.txtContentCard}>18 November 2023</Text>
          </View>
        </View>
        <View style={styles.containerCardContent}>
          {renderButtonStatus(status)}
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              props.onPress();
            }}
            style={[
              styles.cardStatus,
              {
                backgroundColor: color.primary,
                flexDirection: 'row',
                paddingVertical: 11,
                justifyContent: 'space-between',
              },
            ]}>
            <Text
              style={{
                color: color.white,
                fontFamily: fonts.popinsLight,
                fontSize: 13,
                marginLeft: 20,
              }}>
              {props.titleButton}
            </Text>
            <Ionicons
              name="chevron-forward-outline"
              color={color.white}
              size={18}
              style={{marginRight: 8}}
            />
            {/* </View> */}
          </TouchableOpacity>
        </View>
      </View>
      <View style={{height: 20}} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    marginHorizontal: 23,
  },
  itemContent: {
    marginTop: 18,
  },
  txtLabeltitle: {
    color: color.black,
    fontSize: 18,
    fontFamily: fonts.popinsBold,
  },
  txtColorPrimary: {
    color: color.primary,
    fontSize: 14,
    fontFamily: fonts.popinsBold,
  },
  mainContent: {
    flex: 1,
    paddingTop: 30,
  },
  childContent: {
    flexDirection: 'column',
    paddingVertical: 25,
  },
  gapFlex: {
    flex: 1,
  },
  rowContent: {
    flexDirection: 'row',
  },
  containerMain: {
    paddingHorizontal: 23,
    paddingBottom: 23,
    paddingTop: 13,
    marginBottom: 36,
  },
  containerTopLabel: {
    paddingTop: 26,
    paddingLeft: 30,
    marginBottom: 13,
  },
  txtSuccess: {
    color: color.labelGreen,
  },
  txtLabelBlack: {
    fontSize: 15,
    fontFamily: fonts.popinsBold,
    color: color.black,
  },
  txtLabelPrimary: {
    fontSize: 15,
    fontWeight: '600',
    color: color.primary,
  },

  txtLabelGreen: {
    color: color.labelGreen,
  },
  txtLabelRed: {
    color: color.labelRed,
  },
  txtLabelYellow: {
    color: color.labelWarning,
  },
  txtLabelTop: {
    fontSize: 15,
    fontWeight: '600',
    color: color.black,
  },
  img: {
    width: 430,
    height: 154,
  },
  txtContent: {
    fontSize: 15,
    fontFamily: fonts.popinsLight,
  },
  txtButtonCard: {
    fontSize: 14,
    fontFamily: fonts.popinsBold,
  },
  contentCardBottom: {
    marginTop: 28,
    paddingLeft: 30,
    flexDirection: 'row',
  },
  contentBtnCard: {
    paddingHorizontal: 40,
    height: 35,
    borderRadius: 4,
  },
  txtProfile: {
    fontFamily: fonts.interBold,
    color: color.white,
    fontSize: 18,
  },
  txtProfileContent: {
    fontFamily: fonts.interLight,
    color: 'rgba(255, 255, 255, 0.70)',
    fontSize: 16,
  },
  menuDashboard: {
    marginHorizontal: 20,
    backgroundColor: color.white,
    paddingVertical: 12,
    paddingHorizontal: 10,
    marginTop: -90,
    // top: 90,
    // position: "absolute",
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderRadius: 12,
  },

  menuChild: {
    width: SCREEN_WIDTH / 4,
    justifyContent: 'center',
    alignContent: 'space-between',
    alignItems: 'center',
    marginVertical: 12,
  },
  containerCard: {
    flexDirection: 'row',
    paddingLeft: 29,
    paddingRight: 42,
    paddingTop: 28,
  },
  txtCard: {
    fontSize: 18,
    color: color.white,
    fontFamily: fonts.popinsBold,
  },
  imgCard: {
    width: '100%',
    height: 174,
    borderRadius: 20,
    overflow: 'hidden',
  },
  txtTitle: {
    fontFamily: fonts.popinsBold,
    color: color.black,
    fontSize: 18,
  },
  cardStatus: {
    flex: 1,
    borderRadius: 10,
    marginRight: 15,
  },
  cardLabel: {
    fontFamily: fonts.popinsLight,
    paddingVertical: 11,
    textAlign: 'center',
    fontSize: 13,
  },
  cardLabelBtn: {
    fontFamily: fonts.popinsMedium,
    paddingVertical: 11,
    paddingHorizontal: 2,
    fontSize: 15,
  },
  btnForward: {
    backgroundColor: color.rgbaPrimary,
    paddingLeft: 9,
    paddingRight: 7,
    paddingVertical: 8,
    borderRadius: 10,
    marginLeft: 10,
  },
  txtTitleCard: {
    color: '#808080',
    fontSize: 13,
    fontFamily: fonts.popinsLight,
  },
  txtContentCard: {
    fontFamily: fonts.popinsLight,
    color: color.black,
    fontSize: 13,
  },
  containerCardContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 8,
  },
});
