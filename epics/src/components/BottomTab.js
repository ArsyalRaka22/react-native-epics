import React from 'react';
import {Dimensions, Image} from 'react-native';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import color from '../utils/color';
import {useSelector} from 'react-redux';
import {fonts} from '../utils/fonts';

const SCREEN_WIDTH = Dimensions.get('window').width;

const MENUS = [
  {
    label: 'Beranda',
    img: require('../assets/bottomIcons/Home.png'),
    target: 'Beranda',
  },
  {
    label: 'Pelacakan',
    img: require('../assets/bottomIcons/logo_scanner.png'),
    target: 'Pelacakan',
  },
  {
    label: 'Profil',
    img: require('../assets/bottomIcons/User.png'),
    target: 'Profile',
  },
];

export default function BottomTab(props) {
  const user = useSelector(state => state.user);
  return (
    <View style={styles.wrapper}>
      {/* {user.role === 'Pemohon' && (
        <> */}
      {MENUS.map((menu, key) => {
        const isActive = props.selected === key;
        let buttonStyle = [styles.buttonInternal];
        let contentColor = color.black;
        let fontWidth = '500';

        if (isActive) {
          // @ts-ignore
          buttonStyle = [styles.buttonInternal];
          contentColor = color.primary;
          fontWidth = '800';
        }
        if (menu.label === 'Pelacakan') {
          return (
            <TouchableOpacity
              style={styles.button}
              key={key}
              onPress={() => {
                props.onClick(menu.target);
              }}>
              <View style={buttonStyle}>
                <Image
                  source={menu?.img}
                  style={[
                    styles.menuImg,
                    {height: 57, width: 57, position: 'absolute', top: -40},
                  ]}
                  resizeMode="cover"
                />
                <Text
                  style={[
                    styles.textActive,
                    {color: contentColor, marginTop: 25},
                  ]}>
                  {menu.label}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }
        return (
          <TouchableOpacity
            style={styles.button}
            key={key}
            onPress={() => {
              props.onClick(menu.target);
            }}>
            <View style={buttonStyle}>
              <Image
                source={menu?.img}
                style={styles.menuImg}
                resizeMode="cover"
              />
              <Text style={[styles.textActive, {color: contentColor}]}>
                {menu.label}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
      {/* </>
      )} */}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: color.white,
    elevation: 5,
    paddingTop: 14,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
  button: {
    flex: 1,
    paddingHorizontal: 4,
    paddingVertical: 10,
    flexDirection: 'column',
  },
  buttonInternal: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 50,
  },
  buttonBig: {
    position: 'absolute',
    left: SCREEN_WIDTH / 2 - 50,
    bottom: 0,
    width: 100,
    borderRadius: 57,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    tintColor: color.grayColor,
  },
  iconActive: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
    tintColor: color.primary,
    marginBottom: 5,
  },
  text: {
    fontSize: 15,
    textAlign: 'center',
  },
  textActive: {
    fontSize: 15,
    textAlign: 'center',
    // color: color.primary,
    marginTop: 4,
    fontFamily: fonts.popinsMedium,
  },
  bigIcon: {
    width: 100,
    height: 100,
  },
  menuImg: {
    width: 24,
    height: 24,
  },
});
