import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import color from '../utils/color';
import {fonts} from '../utils/fonts';

export default function HeaderBack(props) {
  return (
    <View style={styles.container}>
      <View style={{backgroundColor: color.primary}}>
        <View style={styles.containerHeader}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => props.onPress()}
            style={styles.containerIconLeft}>
            <Image
              source={require('../assets/right.png')}
              style={styles.iconLeft}
              resizeMode="cover"
            />
          </TouchableOpacity>
          <Text style={styles.txtTitle}>{props.title}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 71,
    backgroundColor: color.primary,
    justifyContent: 'center',
  },
  img: {
    position: 'absolute',
    width: 430,
    height: 71,
  },
  txtTitle: {
    fontSize: 18,
    fontFamily: fonts.montserratBold,
    fontWeight: '600',
    color: color.white,
  },
  containerHeader: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  iconLeft: {
    width: 26,
    height: 26,
  },
  containerIconLeft: {
    paddingHorizontal: 27,
    marginVertical: 26,
  },
});
