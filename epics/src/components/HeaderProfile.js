import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Image,
  ImageBackground,
} from 'react-native';
import color from '../utils/color';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default function HeaderProfile(props) {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user);

  const isFocused = useIsFocused();
  const [isCount, setCount] = useState(0);

  const [isLoading, setIsLoading] = useState(false);
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const [showPinInputModal, setShowPinInputModal] = useState(false);
  const [value, setValue] = useState('');

  return (
    <>
      <SafeAreaView style={{backgroundColor: color.primary}} />
      <ImageBackground source={require('../assets/background_beranda.png')}>
        <View style={{height: SCREEN_HEIGHT / 4}}>
          <View
            style={[
              styles.container,
              {
                flex: 1,
                alignContent: 'center',
              },
            ]}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={props.iconProfile}
              style={[
                styles.containerProfile,
                {overflow: 'hidden', alignSelf: 'center', top: 29},
              ]}>
              {/* <Ionicons name="person-outline" size={44} color={color.black} /> */}
              <Image
                source={require('../assets/dumy_foto.png')}
                style={{height: '100%', width: '100%', borderRadius: 50}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </>
  );
}

const styles = {
  container: {
    zIndex: 20,
    flexDirection: 'column',
    width: SCREEN_WIDTH,
    paddingVertical: 16,
    paddingHorizontal: 20,
  },
  containerProfile: {
    // borderWidth: 1,
    // borderColor: color.gray,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: color.white,
    width: 78,
    height: 78,
    borderRadius: 50,
  },

  containerText: {
    flex: 1,
    paddingHorizontal: 10,
  },

  menuRight: {
    width: 38,
    height: 38,
    borderRadius: 12,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#224B7A',
  },
};
