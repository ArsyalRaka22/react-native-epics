import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import color from '../utils/color';
import {useSelector} from 'react-redux';
import {fonts} from '../utils/fonts';

const MENU_OPERATOR = [
  {
    title: 'Monitoring',
    img: require('../assets/dashboard/Operator/monitoring.png'),
    target: 'MenuMonitoring',
  },
  {
    title: 'Perizinan',
    img: require('../assets/dashboard/Operator/perizinan.png'),
    target: 'MenuPerizinan',
  },
  {
    title: 'Arsip',
    img: require('../assets/dashboard/Operator/arsip.png'),
    target: 'MenuArsip',
  },
  {
    title: 'Chat',
    img: require('../assets/dashboard/Operator/chat.png'),
    target: 'Chat',
  },
];
const MENU_VERIFIKATOR = [
  {
    title: 'Monitoring',
    img: require('../assets/dashboard/Operator/monitoring.png'),
    target: 'MenuMonitoring',
  },
  {
    title: 'Perizinan',
    img: require('../assets/dashboard/Operator/perizinan.png'),
    target: 'MenuPerizinan',
  },
  {
    title: 'Arsip',
    img: require('../assets/dashboard/Operator/arsip.png'),
    target: 'MenuArsip',
  },
  {
    title: 'Survey',
    img: require('../assets/dashboard/Operator/survey.png'),
    target: 'MenuSurvey',
  },
];
const MENU_SURVEYOR = [
  {
    title: 'Monitoring',
    img: require('../assets/dashboard/Operator/monitoring.png'),
    target: 'MenuMonitoring',
  },
  {
    title: 'Chat',
    img: require('../assets/dashboard/Operator/chat.png'),
    target: 'Chat',
  },
  {
    title: 'Arsip',
    img: require('../assets/dashboard/Operator/arsip.png'),
    target: 'MenuArsip',
  },
  {
    title: 'Survey',
    img: require('../assets/dashboard/Operator/survey.png'),
    target: 'MenuSurvey',
  },
];

export default function MenuDashboard(props) {
  const user = useSelector(state => state.user);
  return (
    <View style={styles.container}>
      <View style={styles.containerMenu}>
        {user.role === 'Operator' && (
          <>
            {MENU_OPERATOR.map((item, index) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    props.onClick(item.target);
                  }}
                  style={styles.containerItemMenu}
                  key={index}>
                  <Image
                    source={item.img}
                    style={styles.img}
                    resizeMode="cover"
                  />
                  <Text style={styles.itemTitle} numberOfLines={2}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </>
        )}
        {user.role === 'Verifikator' && (
          <>
            {MENU_VERIFIKATOR.map((item, index) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    props.onClick(item.target);
                  }}
                  style={styles.containerItemMenu}
                  key={index}>
                  <Image
                    source={item.img}
                    style={styles.img}
                    resizeMode="cover"
                  />
                  <Text style={styles.itemTitle} numberOfLines={2}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </>
        )}
        {user.role === 'Surveyor' && (
          <>
            {MENU_SURVEYOR.map((item, index) => {
              return (
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    props.onClick(item.target);
                  }}
                  style={styles.containerItemMenu}
                  key={index}>
                  <Image
                    source={item.img}
                    style={styles.img}
                    resizeMode="cover"
                  />
                  <Text style={styles.itemTitle} numberOfLines={2}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: color.black,
    flexDirection: 'row',
    marginHorizontal: 27,
    marginTop: 20,
  },
  containerMenu: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // gap: 38,
  },
  containerItemMenu: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: 58,
    height: 58,
  },
  itemTitle: {
    alignSelf: 'center',
    marginTop: 14,
    fontSize: 14,
    fontFamily: fonts.montserratReguler,
    color: color.black,
  },
});
