import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import color from '../utils/color';
import {fonts} from '../utils/fonts';

export default function HeaderTitle(props) {
  return (
    <View style={styles.container}>
      {/* <ImageBackground
        source={require('../assets/dashboard.png')}
        style={styles.img}
        resizeMode="cover"
      /> */}
      <View style={styles.containerHeader}>
        <Text style={styles.txtTitle}>{props.title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 71,
    backgroundColor: color.primary,
    justifyContent: 'center',
  },
  img: {
    position: 'absolute',
    width: 430,
    height: 71,
  },
  txtTitle: {
    fontSize: 20,
    fontWeight: '600',
    fontFamily: fonts.montserratBold,
    color: color.black,
    alignSelf: 'center',
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconLeft: {
    width: 20,
    height: 20,
  },
  containerIconLeft: {
    paddingHorizontal: 27,
    marginVertical: 26,
  },
});
