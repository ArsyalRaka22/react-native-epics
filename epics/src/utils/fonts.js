export const fonts = {
  montserratBlack: 'Montserrat-Black',
  montserratReguler: 'Montserrat-Regular',
  montserratBold: 'Montserrat-Bold',
  popins: 'Poppins-Black',
  popinsLight: 'Poppins-Light',
  popinsBold: 'Poppins-Bold',
  popinsExtraBold: 'Poppins-ExtraBold',
  popinsMedium: 'Poppins-Medium',
  interLight: 'Inter-Light',
  interBlack: 'Inter-Black',
  interBold: 'Inter-Bold',
  interExtraBold: 'Inter-ExtraBold',
};
