export default {
  primary: '#499DB1',
  white: '#FFFFFF',
  black: '#000000',
  black25: '#cccccc',
  black50: '#7d7d7d',

  gray: '#CCCCCC',
  danger: '#e74c3c',
  logout: '#FF6060',
  success: '#2ecc71',

  Neutral80: '#A5A5A5',
  Neutral20: '#F6F6F6',
  Neutral10: '#6C6C6C',

  themeGray: '#e9e9e9',

  grayColor: '#F3F3F3',
  greenColor: '#007427',
  orangeColor: '#FD6822',
  yellowColor: '#FFBD49',
  whiteColorRgba: 'rgba(255, 255, 255, 0.2)',

  labelGreen: '#55CF53',
  labelRed: '#FF7575',
  labelWarning: '#FAB754',
  rgbaWarning: 'rgba(252, 210, 61, 0.1)',
  rgbaGreen: 'rgba(85, 207, 83, 0.1)',
  rgbaRed: 'rgba(255, 117, 117, 0.1)',
  rgbaGray: 'rgba(77, 155, 174, 0.1)',
  rgbaMainGray: 'rgba(77, 155, 174, 1)',
  rgbaPrimary: '#5DB7CC',
  rgbaBlue: '#6096FF',

  semua_perizinan: 'rgba(185, 174, 255, 1)',
  perizinan_masuk: 'rgba(163, 239, 184, 1)',
  perizinan_terlambat: 'rgba(240, 164, 164, 1)',
  perizinan_dikembalikan: 'rgba(240, 210, 164, 1)',
  perizinan_diterbitkan: 'rgba(163, 224, 237, 1)',
};
