import React from 'react';
import {useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from '../screens/SplashScreen';
import Login from '../screens/Login';
import Beranda from '../screens/Beranda';
import LupaKataSandi from '../screens/LupaKataSandi';
import Register from '../screens/Register';
import Intro from '../screens/Intro';
import Pelacakan from '../screens/Pelacakan';
import Profile from '../screens/Profile';
import Chat from '../screens/Chat';
import Notifikasi from '../screens/Notifikasi';
import MenuAjukan from '../screens/MenuAjukan';
import PelacakanById from '../screens/PelacakanById';
import DetailChat from '../screens/DetailChat';
import KepuasanPelanggan from '../screens/KepuasanPelanggan';
import MenuPerizinan from '../screens/MenuPerizinan';
import MenuPersyaratan from '../screens/MenuPersyaratan';
import MenuArsip from '../screens/MenuArsip';
import AjukanNext from '../screens/AjukanNext';
import DetailPerizinan from '../screens/DetailPerizinan';
import Faq from '../screens/Faq';
import MenuMonitoring from '../screens/MenuMonitoring';
import DaftarVideoTutorial from '../screens/DaftarVideoTutorial';
import SemuaPerizinan from '../screens/SemuaPerizinan';
import PerizinanMasuk from '../screens/PerizinanMasuk';
import PerizinanTerlambaat from '../screens/PerizinanTerlambat';
import PerizinanDikembalikan from '../screens/PerizinanDikembalikan';
import PerizinanDiterbitkan from '../screens/PerizinanDiterbitkan';
import MenuSurvey from '../screens/MenuSurvey';
import MenuUlasan from '../screens/MenuUlasan';
import KembalikanPerizinan from '../screens/KembalikanPerizinan';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const AuthTabNavigator = () => {
  return (
    <Tab.Navigator initialRouteName="SplashScreen" tabBar={() => null}>
      <Tab.Screen
        name="Intro"
        component={Intro}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Login"
        component={Login}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="LupaKataSandi"
        component={LupaKataSandi}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Register"
        component={Register}
        options={() => ({headerShown: false})}
      />
    </Tab.Navigator>
  );
};

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="AuthTabNavigator"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="AuthTabNavigator" component={AuthTabNavigator} />
    </Stack.Navigator>
  );
};

const DashboardTabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Beranda"
      backBehavior={'none'}
      tabBar={() => null}>
      <Tab.Screen
        name="Beranda"
        component={Beranda}
        options={() => ({headerShown: false})}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={() => ({headerShown: false})}
      />
    </Tab.Navigator>
  );
};

const DashboardStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Dashboard"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Dashboard" component={DashboardTabNavigator} />
      <Stack.Screen name="Notifikasi" component={Notifikasi} />

      <Stack.Screen name="MenuAjukan" component={MenuAjukan} />
      <Stack.Screen name="AjukanNext" component={AjukanNext} />
      <Stack.Screen name="MenuPerizinan" component={MenuPerizinan} />
      <Stack.Screen name="MenuPersyaratan" component={MenuPersyaratan} />
      <Stack.Screen name="MenuArsip" component={MenuArsip} />

      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="DetailChat" component={DetailChat} />

      <Stack.Screen name="Pelacakan" component={Pelacakan} />
      <Stack.Screen name="PelacakanById" component={PelacakanById} />

      <Stack.Screen name="KepuasanPelanggan" component={KepuasanPelanggan} />
      <Stack.Screen name="DetailPerizinan" component={DetailPerizinan} />

      <Stack.Screen name="Faq" component={Faq} />
      <Stack.Screen name="MenuMonitoring" component={MenuMonitoring} />
      <Stack.Screen
        name="DaftarVideoTutorial"
        component={DaftarVideoTutorial}
      />

      <Stack.Screen name="SemuaPerizinan" component={SemuaPerizinan} />
      <Stack.Screen name="PerizinanMasuk" component={PerizinanMasuk} />
      <Stack.Screen name="PerizinanTerlambat" component={PerizinanTerlambaat} />
      <Stack.Screen
        name="PerizinanDikembalikan"
        component={PerizinanDikembalikan}
      />
      <Stack.Screen
        name="PerizinanDiterbitkan"
        component={PerizinanDiterbitkan}
      />
      <Stack.Screen name="MenuSurvey" component={MenuSurvey} />
      <Stack.Screen name="MenuUlasan" component={MenuUlasan} />
      <Stack.Screen
        name="KembalikanPerizinan"
        component={KembalikanPerizinan}
      />
    </Stack.Navigator>
  );
};

const AppContainer = () => {
  const user = useSelector(state => state.user);
  const splash = useSelector(state => state.splash);

  if (splash === true) {
    return <SplashScreen />;
  }

  if (user != null) {
    return (
      <>
        <NavigationContainer>
          <Tab.Navigator tabBar={() => null}>
            <Tab.Screen
              name="Dashboard"
              component={DashboardStack}
              options={() => ({headerShown: false})}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </>
    );
  } else {
    return (
      <NavigationContainer>
        <Tab.Navigator tabBar={() => null}>
          <Tab.Screen
            name="Auth"
            component={AuthNavigator}
            options={() => ({headerShown: false})}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
};

export default AppContainer;
