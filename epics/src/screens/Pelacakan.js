import React, {useCallback} from 'react';

import {AppRegistry, StyleSheet, Text, Alert, View} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import {ScrollView} from 'react-native-gesture-handler';
import BottomTab from '../components/BottomTab';
import {useSelector} from 'react-redux';
import Button from '../components/Button';
import color from '../utils/color';

export default function Pelacakan(props) {
  const navigation = useNavigation();
  const user = useSelector(state => state.user);
  const onSuccess = useCallback(e => {
    if (e.type == 'QR_CODE') {
      // pindahPage(e.data);
      console.log('a', e.data);
    } else {
      Alert.alert('Informasi', 'Maaf ini bukan qrcode');
    }
  }, []);

  const pindahPage = useCallback(value => {
    // navigation.navigate('DetailKantin', {params: value});
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: color.white}}>
      {/* <View style={{position: 'absolute'}}> */}
      <HeaderBack
        title="ScanQRCODE"
        onPress={() => navigation.navigate('Beranda')}
      />
      <View style={{flex: 1}}>
        <QRCodeScanner
          reactivate={true}
          showMarker={true}
          onRead={onSuccess}
          // flashMode={RNCamera.Constants.FlashMode.torch}
          // topContent={
          // <Text style={styles.centerText}>
          //   <Text style={styles.textBold}>Scan</Text>
          // </Text>
          // <HeaderBack title="Chat" onPress={() => navigation.goBack()} />
          // }
          // bottomContent={
          //     <TouchableOpacity style={styles.buttonTouchable}>
          //         <Text style={styles.buttonText}>OK. Got it!</Text>
          //     </TouchableOpacity>
          // }
        />
      </View>
      {/* </View> */}
      {/* {user.role === 'Operator' && ( */}
      <View style={styles.wrapperButton}>
        <Button
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('PelacakanById');
          }}>
          ID Surat
        </Button>
      </View>
      {/* )} */}
    </View>
  );
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  wrapperButton: {
    backgroundColor: color.white,
    paddingVertical: 50,
    paddingHorizontal: 23,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
});
