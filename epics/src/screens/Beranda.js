import React, {useCallback} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ImageBackground,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderBeranda from '../components/HeaderBeranda';
import color from '../utils/color';
import HeaderPopUp from '../components/HeaderPopUp';
import {useSelector} from 'react-redux';
import {fonts} from '../utils/fonts';
import {ScrollView} from 'react-native-gesture-handler';
import app from '../config/app';
import HeaderTablet from '../components/HeaderTablet';
import Button from '../components/Button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ButtonCard from '../components/ButtonCard';
import CardList from '../components/CardList';
import BerandaCardPanduan from '../components/BerandaCardPanduan';
import MenuDashboard from '../components/MenuDashboard';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default function Beranda(props) {
  const navigation = useNavigation();
  const user = useSelector(state => state.user);

  const menuPemohon = [
    {
      name: 'Ajukan',
      image: require('../assets/menuDashboard/menu_ajukan.png'),
      page: 'MenuAjukan',
    },
    {
      name: 'Perizinan',
      image: require('../assets/menuDashboard/menu_perizinan.png'),
      page: 'MenuPerizinan',
    },
    {
      name: 'Persyaratan',
      image: require('../assets/menuDashboard/menu_persyaratan.png'),
      page: 'MenuPersyaratan',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
    {
      name: 'Chat',
      image: require('../assets/menuDashboard/menu_chat.png'),
      page: 'Chat',
    },
    {
      name: 'Penilaian App',
      image: require('../assets/menuDashboard/menu_penilaian.png'),
      page: 'KepuasanPelanggan',
    },
  ];

  const MENU_KEPALA_DINAS = [
    {
      name: 'Perizinan',
      image: require('../assets/menuDashboard/menu_perizinan.png'),
      page: 'MenuPerizinan',
    },
    {
      name: 'Monitoring',
      image: require('../assets/dashboard/Operator/monitoring.png'),
      page: 'MenuMonitoring',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
    {
      name: 'Ulasan',
      image: require('../assets/dashboard/Operator/ulasan.png'),
      page: '',
    },
  ];
  const MENU_AUDITS = [
    {
      name: 'Monitoring',
      image: require('../assets/dashboard/Operator/monitoring.png'),
      page: 'MenuMonitoring',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
    {
      name: 'Ulasan',
      image: require('../assets/dashboard/Operator/ulasan.png'),
      page: 'MenuUlasan',
    },
  ];
  const MENU_WALIKOTA = [
    {
      name: 'Monitoring',
      image: require('../assets/dashboard/Operator/monitoring.png'),
      page: 'MenuMonitoring',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
  ];

  const MENU_ADMIN_UTAMA = [
    {
      name: 'Monitoring',
      image: require('../assets/dashboard/Operator/monitoring.png'),
      page: 'MenuMonitoring',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
    {
      name: 'Ulasan',
      image: require('../assets/dashboard/Operator/ulasan.png'),
      page: 'MenuUlasan',
    },
  ];
  const MENU_ADMIN_DINAS = [
    {
      name: 'Monitoring',
      image: require('../assets/dashboard/Operator/monitoring.png'),
      page: 'MenuMonitoring',
    },
    {
      name: 'Arsip',
      image: require('../assets/menuDashboard/menu_arsip.png'),
      page: 'MenuArsip',
    },
    {
      name: 'Ulasan',
      image: require('../assets/dashboard/Operator/ulasan.png'),
      page: 'MenuUlasan',
    },
  ];

  const data = [
    {
      status: 1,
    },
    {
      status: 2,
    },
    {
      status: 3,
    },
  ];

  return (
    <>
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor={color.primary} barStyle="dark-content" />
        <ScrollView style={{flex: 1}}>
          <HeaderTablet
            textHeader={app.NAME}
            textProfile={styles.txtProfile}
            textAlamat={styles.txtProfileContent}
            // iconProfile={() => {
            //     navigation.navigate("Profile")
            // }}
            iconRight={() => {
              navigation.navigate('Notifikasi');
            }}
          />

          <View style={{zIndex: 1}}>
            {user.role === 'Pemohon' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {menuPemohon.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={styles.menuChild}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'KepalaDinas' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {MENU_KEPALA_DINAS.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={[styles.menuChild, {width: SCREEN_WIDTH / 5}]}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'Auditor' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {MENU_AUDITS.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={[styles.menuChild, {width: SCREEN_WIDTH / 4}]}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'Bupati' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {MENU_WALIKOTA.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={[styles.menuChild, {width: SCREEN_WIDTH / 3}]}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'AdminUtama' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {MENU_ADMIN_UTAMA.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={[styles.menuChild, {width: SCREEN_WIDTH / 4}]}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'AdminDinas' && (
              <>
                <View style={[styles.menuDashboard]}>
                  {MENU_ADMIN_DINAS.map((item, ilist) => {
                    return (
                      <>
                        <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => {
                            if (item.page !== '') {
                              navigation.navigate(item.page);
                            }
                          }}
                          style={[styles.menuChild, {width: SCREEN_WIDTH / 4}]}>
                          <View
                            style={[
                              styles.menuIcon,
                              {
                                backgroundColor: item.warna,
                              },
                            ]}>
                            <Image
                              source={item.image}
                              style={{width: 58, height: 58}}
                            />
                          </View>
                          <Text
                            style={{
                              textAlign: 'center',
                              fontSize: 14,
                              fontFamily: fonts.popinsLight,
                              marginVertical: 12,
                              flex: 1,
                            }}>
                            {item.name}
                          </Text>
                        </TouchableOpacity>
                      </>
                    );
                  })}
                </View>
              </>
            )}
            {user.role === 'Operator' && <HeaderPopUp />}
            {user.role === 'Operator' && (
              <MenuDashboard
                onClick={event => {
                  navigation.navigate(event);
                }}
              />
            )}
            {/*  */}
            {user.role === 'Verifikator' && <HeaderPopUp />}
            {user.role === 'Verifikator' && (
              <MenuDashboard
                onClick={event => {
                  if (event != '') {
                    navigation.navigate(event);
                  }
                }}
              />
            )}

            {user.role === 'Surveyor' && <HeaderPopUp />}
            {user.role === 'Surveyor' && (
              <MenuDashboard
                onClick={event => {
                  if (event != '') {
                    navigation.navigate(event);
                  }
                }}
              />
            )}

            <View style={styles.mainWrapperContent}>
              <BerandaCardPanduan
                title={
                  user.role === 'Pemohon'
                    ? 'Video Panduan Pengajuan Berkas'
                    : 'Daftar Video Panduan'
                }
                titleButton={
                  user.role === 'Pemohon' ? 'Lihat Video' : 'Selengkapnya'
                }
                onClick={() => {
                  navigation.navigate('DaftarVideoTutorial');
                }}
              />
              <View style={{height: 29}} />
              {/*  */}
              <View style={{flexDirection: 'row', marginBottom: 26}}>
                <Image
                  source={require('../assets/logo_list.png')}
                  style={{height: 25, width: 20}}
                />
                <View style={{marginLeft: 13}}>
                  <Text style={styles.txtTitle}>Permohonan Terbaru</Text>
                </View>
              </View>
              {/*  */}
              {data.map(item => {
                return (
                  <>
                    <CardList
                      data={item}
                      status={item.status}
                      titleButton={'Detail Surat'}
                      onPress={() => {}}
                    />
                  </>
                );
              })}
            </View>
          </View>
          <View style={{height: 26}} />
        </ScrollView>

        <BottomTab
          selected={0}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },

  menuDashboard: {
    marginHorizontal: 20,
    backgroundColor: color.white,
    paddingVertical: 12,
    paddingHorizontal: 10,
    marginTop: -90,
    // top: 90,
    // position: "absolute",
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderRadius: 12,
    elevation: 5,
  },

  menuChild: {
    width: SCREEN_WIDTH / 4,
    justifyContent: 'center',
    alignContent: 'space-between',
    alignItems: 'center',
    marginVertical: 12,
  },

  mainWrapperContent: {
    paddingHorizontal: 27,
    paddingTop: 26,
  },

  txtProfile: {
    fontFamily: fonts.montserratReguler,
    color: color.white,
    fontSize: 18,
  },
  txtProfileContent: {
    fontFamily: fonts.montserratReguler,
    color: 'rgba(255, 255, 255, 0.70)',
    fontSize: 18,
  },
  txtTitle: {
    fontFamily: fonts.montserratBold,
    color: color.black,
    fontWeight: '600',
    fontSize: 18,
  },
});
