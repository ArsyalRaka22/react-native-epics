import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import Button from '../components/Button';
import color from '../utils/color';
import app from '../config/app';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import {setUser} from '../store/actions';
import {ScrollView} from 'react-native-gesture-handler';
import DatePicker from '../components/DatePicker';
import Combobox from '../components/Combobox';
import HeaderTransparent from '../components/HeaderTransparent';

export default function Register({navigation}) {
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [email, setEmail] = useState(__DEV__ ? app.EXAMPLE_EMAIL : '');
  const [password, setPassword] = useState(__DEV__ ? app.EXAMPLE_PASSWORD : '');
  const [password2, setPassword2] = useState(
    __DEV__ ? app.EXAMPLE_PASSWORD : '',
  );
  const [nama, setNama] = useState('');
  const [tanggalLahir, setTanggaLahir] = useState(new Date());
  const [nomorIdentitas, setNomorIdentitas] = useState('');
  const [nomortelp, setNomorTelp] = useState('');
  const [alamat, setAlamat] = useState('');
  const [kota, setKota] = useState('');
  const [provinsi, setProvinsi] = useState('');
  const [kecamatan, setKecamatan] = useState('');
  const [kelurahan, setKelurahan] = useState('');
  const [pekerjaan, setPekerjaan] = useState('');

  const toggleSetDay = useCallback(tanggal => {
    setTanggaLahir(tanggal);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.content}>
        <HeaderTransparent />
        <View style={{alignSelf: 'center', marginBottom: 38, marginTop: 18}}>
          <Text style={styles.headerTitle}>Daftar Epic</Text>
        </View>
        <View style={{paddingHorizontal: 20}}>
          <Text style={styles.label}>Nama Lengkap</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Nama"
            value={nama}
            onChangeText={setNama}
            containerStyle={styles.input}
          />

          <Text style={styles.label}>Tanggal Lahir</Text>
          <DatePicker
            format="YYYY-MM-DD"
            displayFormat="DD MMM YYYY"
            nameLabel="tanggal"
            value={tanggalLahir}
            onChange={tanggal => {
              toggleSetDay(tanggal);
            }}
          />
          <View style={styles.gapY} />
          <Combobox
            showSearchBar={true}
            label={'Jenis Kelamin'}
            placeholder="Silahkan Pilih"
            value={''}
            theme={{
              boxStyle: {
                backgroundColor: color.white,
                paddingLeft: 30,
                borderRadius: 12,
              },
              leftIconStyle: {
                color: color.black,
                marginRight: 14,
              },
              rightIconStyle: {
                color: color.black,
              },
            }}
            data={[
              {
                id: 'L',
                label: 'Laki - Laki',
              },
              {
                id: 'P',
                label: 'Perempuan',
              },
            ]}
            jenisIconsRight="Ionicons"
            iconNameRight="caret-down-outline"
            showLeftIcons={false}
            onChange={val => {
              // setSelectedPelanggan(val)
              console.log('val', val);
            }}
          />
          <View style={styles.gapY} />
          <Combobox
            showSearchBar={true}
            label={'Jenis Identitas'}
            placeholder="Silahkan Pilih"
            value={''}
            theme={{
              boxStyle: {
                backgroundColor: color.white,
                paddingLeft: 30,
                borderRadius: 12,
              },
              leftIconStyle: {
                color: color.black,
                marginRight: 14,
              },
              rightIconStyle: {
                color: color.black,
              },
            }}
            data={[
              {
                id: '1',
                label: 'KTP',
              },
            ]}
            jenisIconsRight="Ionicons"
            iconNameRight="caret-down-outline"
            showLeftIcons={false}
            onChange={val => {
              // setSelectedPelanggan(val)
              console.log('val', val);
            }}
          />
          <View style={styles.gapY} />
          <Text style={styles.label}>Nomor Identitas</Text>

          <TextInputIcon
            isIcon={true}
            placeholder="Nomor Identitas"
            value={nomorIdentitas}
            onChangeText={setNomorIdentitas}
            secureTextEntry={false}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Nomor Telp</Text>

          <TextInputIcon
            isIcon={true}
            placeholder="Nomor Telp"
            value={nomortelp}
            onChangeText={setNomorTelp}
            secureTextEntry={false}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Alamat</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Alamat"
            value={alamat}
            onChangeText={setAlamat}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Provinsi</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Provinsi"
            value={provinsi}
            onChangeText={setProvinsi}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Kota</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Kota"
            value={kota}
            onChangeText={setKota}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Kecamatan</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Kecamatan"
            value={kecamatan}
            onChangeText={setKecamatan}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Kelurahan</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Kelurahan"
            value={kelurahan}
            onChangeText={setKelurahan}
            containerStyle={styles.input}
          />

          <Text style={styles.label}>Email</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
            containerStyle={styles.input}
          />

          <Text style={styles.label}>Password</Text>

          <TextInputIcon
            isIcon={true}
            placeholder="Password"
            value={password}
            onChangeText={setPassword}
            secureTextEntry={true}
            containerStyle={styles.input}
          />
          <Text style={styles.label}>Ketik Ulang Sandi</Text>

          <TextInputIcon
            isIcon={true}
            placeholder="Ketik Ulang Sandi"
            value={password2}
            onChangeText={setPassword2}
            secureTextEntry={true}
            containerStyle={styles.input}
          />
          <View style={{height: 56}} />
          <View
            style={{
              flexDirection: 'column',
              marginBottom: 28,
            }}>
            <Button loading={isLoading} onPress={() => dispatch(setUser([]))}>
              Daftar
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  content: {
    flex: 1,
  },
  titleName: {
    fontSize: 20,
    fontFamily: fonts.popinsLight,
    marginBottom: 20,
  },
  input: {
    marginBottom: 14,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 5,
    color: color.black,
  },

  bottomWrap: {
    flex: 4,
    paddingHorizontal: 20,
  },

  lineWrap: {
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#333333',
  },
  textBetween: {
    color: color.white,
    marginHorizontal: 10,
    fontSize: 14,
  },

  buttonNoBg: {
    backgroundColor: color.black,
    // marginBottom: 30,
    marginHorizontal: 20,
    borderWidth: 0,
  },
  signInText: {
    fontWeight: 'bold',
    color: color.primary,
  },

  imageWallpaper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },

  footerForm: {
    flexDirection: 'row',
    marginBottom: 26,
    alignItems: 'center',
  },
  checkBox: {
    width: 24,
    height: 24,
  },
  txtForgotPassword: {
    fontSize: 16,
    textAlign: 'right',
    color: color.primary,
    fontFamily: fonts.popinsLight,
  },
  txtRememberMe: {
    fontSize: 16,
    flex: 1,
    marginLeft: 8,
    color: color.Neutral80,
    fontFamily: fonts.montserratReguler,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  gapY: {
    marginBottom: 14,
  },
  headerTitle: {
    fontFamily: fonts.popinsBold,
    fontSize: 32,
    color: color.black,
  },
});
