import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Touchable,
  TouchableOpacity,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function Chat(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Chat" onPress={() => navigation.goBack()} />
        <ScrollView style={{paddingHorizontal: 27, paddingVertical: 26}}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              navigation.navigate('DetailChat');
            }}
            style={styles.mainCardChat}>
            <Image
              source={require('../assets/dumy_foto.png')}
              style={{height: 55, width: 55, borderRadius: 50}}
            />
            <View style={{flex: 1, marginLeft: 17}}>
              <Text>Operator</Text>
              <Text>Secepatnya Ya</Text>
            </View>
            <View
              style={{marginLeft: 17, alignSelf: 'flex-start', marginTop: 17}}>
              <Text>12:30</Text>
            </View>
          </TouchableOpacity>
          {/*  */}
          <View style={{height: 24}} />
          {/*  */}
          <View
            style={{
              backgroundColor: 'rgba(240, 252, 255, 1)',
              flexDirection: 'row',
              paddingHorizontal: 17,
              paddingVertical: 14,
              borderRadius: 20,
              height: 96,
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../assets/dumy_foto.png')}
              style={{height: 55, width: 55, borderRadius: 50}}
            />
            <View style={{flex: 1, marginLeft: 17}}>
              <Text>Surveyor</Text>
              <Text>Secepatnya Ya</Text>
            </View>
            <View
              style={{marginLeft: 17, alignSelf: 'flex-start', marginTop: 17}}>
              <Text>12:30</Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  mainCardChat: {
    backgroundColor: 'rgba(240, 252, 255, 1)',
    flexDirection: 'row',
    paddingHorizontal: 17,
    paddingVertical: 14,
    borderRadius: 20,
    height: 96,
    alignContent: 'center',
    alignItems: 'center',
  },
});
