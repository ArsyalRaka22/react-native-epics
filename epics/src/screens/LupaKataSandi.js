import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Alert,
  StatusBar,
} from 'react-native';
import {useDispatch} from 'react-redux';
import Button from '../components/Button';
import color from '../utils/color';
import app from '../config/app';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import {setUser} from '../store/actions';
import {ScrollView} from 'react-native-gesture-handler';
import Combobox from '../components/Combobox';
import HeaderTransparent from '../components/HeaderTransparent';

export default function LupaKataSandi({navigation}) {
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [email, setEmail] = useState(__DEV__ ? app.EXAMPLE_EMAIL : '');
  const [password, setPassword] = useState(__DEV__ ? app.EXAMPLE_PASSWORD : '');
  const [passwordKonfirmasi, setPasswordKonfirmas] = useState(
    __DEV__ ? app.EXAMPLE_PASSWORD : '',
  );

  // const login = useCallback(() => {
  //     setLoading(true);
  //     let data = { username, password };
  //     HttpRequest.login(data).then((res) => {
  //         console.log("Res", res.data);
  //         Toast.showSuccess("Login Success");
  //         setLoading(false);
  //         dispatch(setUser(res.data));
  //     }).catch((err) => {
  //         console.log(err, err.response);
  //         Toast.showError(err.response.data.message);
  //         setLoading(false);
  //     });
  // }, [username, password]);

  return (
    <SafeAreaView style={styles.container}>
      <HeaderTransparent />
      <ScrollView style={styles.content}>
        <View style={{alignSelf: 'center', marginBottom: 31, marginTop: 10}}>
          <Text style={styles.headerTitle}>Lupa Kata Sandi</Text>
        </View>
        {/* <Text style={styles.titleName}>{app.NAME}</Text>
        <Text style={[styles.titleName, {fontSize: 18, alignSelf: 'center'}]}>
          Masuk
        </Text> */}

        <Text style={styles.label}>Email</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Username"
          value={email}
          onChangeText={setEmail}
          containerStyle={styles.input}
        />

        <Text style={styles.label}>Kata Sandi</Text>

        <TextInputIcon
          isIcon={true}
          placeholder="Kata Sandi"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
          containerStyle={styles.input}
        />

        <Text style={styles.label}>Kata Sandi Baru</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Kata Sandi"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
          containerStyle={styles.input}
        />
      </ScrollView>

      <View style={styles.wrapperButton}>
        <Button
          loading={isLoading}
          onPress={() => {
            // toggleLogin();
          }}>
          Simpan
        </Button>
        {/* <View
          style={[
            styles.containerText,
            {marginBottom: 90, marginTop: 48, alignSelf: 'center'},
          ]}>
          <Text style={styles.txtLabel}>Belum punya akun? </Text>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('Register');
            }}>
            <Text style={styles.txtLabelPrimary}>Daftar Sekarang</Text>
          </TouchableOpacity>
        </View> */}
      </View>

      <View
        style={{
          flexDirection: 'row',
          position: 'absolute',
          bottom: 40,
          alignSelf: 'center',
        }}>
        <Text style={styles.txtLabel}>Ingat Kata Sandi? </Text>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text style={styles.txtLabelPrimary}>Login Sekarang</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  content: {
    paddingHorizontal: 20,
  },
  titleName: {
    fontSize: 20,
    fontFamily: fonts.popinsLight,
    marginBottom: 20,
  },
  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 5,
    color: color.black,
  },

  bottomWrap: {
    flex: 4,
    paddingHorizontal: 20,
  },

  lineWrap: {
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#333333',
  },
  textBetween: {
    color: color.white,
    marginHorizontal: 10,
    fontSize: 14,
  },

  buttonNoBg: {
    backgroundColor: color.black,
    // marginBottom: 30,
    marginHorizontal: 20,
    borderWidth: 0,
  },
  signInText: {
    fontWeight: 'bold',
    color: color.primary,
  },

  imageWallpaper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },

  footerForm: {
    flexDirection: 'row',
    marginBottom: 26,
    alignItems: 'center',
  },
  checkBox: {
    width: 24,
    height: 24,
  },
  txtForgotPassword: {
    fontSize: 16,
    textAlign: 'right',
    color: color.primary,
    fontFamily: fonts.popinsLight,
  },
  txtRememberMe: {
    fontSize: 16,
    flex: 1,
    marginLeft: 8,
    color: color.Neutral80,
    fontFamily: fonts.montserratReguler,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  txtLabel: {
    fontSize: 16,
  },
  txtLabelPrimary: {
    color: color.primary,
    fontSize: 16,
    marginLeft: 5,
    fontFamily: fonts.popinsLight,
  },
  containerText: {
    marginTop: 18,
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapperButton: {
    flexDirection: 'column',
    paddingHorizontal: 37,
    marginBottom: 120,
  },
  headerTitle: {
    fontFamily: fonts.popinsMedium,
    fontSize: 32,
    color: color.black,
  },
  txtLupaSandi: {
    fontFamily: fonts.popinsMedium,
    fontSize: 16,
    color: color.black,
    textAlign: 'right',
  },
});
