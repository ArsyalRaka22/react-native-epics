import React, {useCallback, useState} from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import TextInputIcon from '../components/TextInputIcon';
import color from '../utils/color';
import {fonts} from '../utils/fonts';
import {useSelector} from 'react-redux';
import Button from '../components/Button';
import ModalAlert from '../components/ModalAlert';

export default function KembalikanPerizinan(props) {
  const navigation = useNavigation();
  const [pertanyaan1, setPertanyaan1] = useState('');
  const user = useSelector(state => state.user);
  const [isModalOperator, setIsModalOperator] = useState(false);
  const [isModalVerifikator, setIsModalVerifikator] = useState(false);

  const renderOperator = useCallback(() => {
    return (
      <>
        <View
          style={{
            backgroundColor: color.white,
            paddingVertical: 49,
            paddingHorizontal: 27,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
          }}>
          <Button
            theme="primary"
            activeOpacity={0.7}
            onPress={() => {
              setIsModalOperator(true);
            }}>
            Kirim Sekarang
          </Button>
        </View>
      </>
    );
  }, []);
  const renderVerifikator = useCallback(() => {
    return (
      <>
        <View
          style={{
            backgroundColor: color.white,
            paddingVertical: 49,
            paddingHorizontal: 27,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
          }}>
          <Button
            theme="primary"
            activeOpacity={0.7}
            onPress={() => {
              setIsModalVerifikator(true);
            }}>
            Kirim Sekarang
          </Button>
        </View>
      </>
    );
  }, []);

  const renderModal = useCallback(() => {
    return (
      <>
        <ModalAlert visible={isModalOperator}>
          <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
            <Text style={styles.txtLabelModal}>
              Alasan dan Berkas Berhasil Dikembalikan ke Pemohon
            </Text>
            <Button
              theme={'reverse-primary'}
              onPress={() => {
                setIsModalOperator(false);
                navigation.navigate('Dashboard');
              }}>
              Kembali
            </Button>
          </View>
        </ModalAlert>
        <ModalAlert visible={isModalVerifikator}>
          <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
            <Text style={styles.txtLabelModal}>
              Hasil Survey Berhasil di Approve dan Akan Diproses Kepala Dinas
            </Text>
            <Button
              theme={'reverse-primary'}
              onPress={() => {
                setIsModalVerifikator(false);
                navigation.navigate('Dashboard');
              }}>
              Kembali
            </Button>
          </View>
        </ModalAlert>
      </>
    );
  }, [isModalOperator, isModalVerifikator, navigation]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          //   title={'Tolak' + ' ' + title}
          title="Kembalikan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={styles.mainContainer}>
          <Text style={styles.label}>Alasan Dikembalikan</Text>
          <TextInputIcon
            isIcon={true}
            wrapperStyle={styles.wrapperText}
            multiline={true}
            placeholder="Jawaban"
            value={pertanyaan1}
            onChangeText={setPertanyaan1}
            containerStyle={styles.input}
          />
        </ScrollView>
        {user.role === 'Operator' && <>{renderOperator()}</>}
        {user.role === 'Verifikator' && <>{renderVerifikator()}</>}
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 16,
    fontFamily: fonts.popinsMedium,
    marginBottom: 5,
    color: color.black,
    fontWeight: '600',
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 23,
    paddingVertical: 20,
  },
  wrapperText: {
    height: 258,
    textAlign: 'left',
    alignItems: 'flex-start',
  },
  txtLabelModal: {
    color: color.black,
    fontSize: 18,
    fontFamily: fonts.montserratBold,
    marginBottom: 34,
    textAlign: 'center',
    alignItems: 'center',
  },
});
