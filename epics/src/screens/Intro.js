import React, {useCallback} from 'react';
import {View, Text, Image, StyleSheet, Alert, StatusBar} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';
import {fonts} from '../utils/fonts';
import {useDispatch} from 'react-redux';
import {setUser} from '../store/actions';

export default function Intro(props) {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  // const toggleBtn = useCallback(() => {
  //   if (role === '') {
  //     Alert.alert('Informasi', 'Jenis Role harap dipilih');
  //   } else {
  //     dispatch(setUser({role: role}));
  //   }
  // }, [dispatch, role]);

  return (
    <View style={styles.container}>
      <View style={{position: 'absolute'}}>
        <Image source={require('../assets/splashscreenBackground.png')} />
        <StatusBar
          translucent
          backgroundColor={'transparent'}
          barStyle="light-content"
        />
      </View>
      <View style={styles.containerImage}>
        <Image
          source={require('../assets/stater_logo.png')}
          style={styles.img}
          resizeMode="cover"
        />
      </View>
      <View style={{marginTop: 62, paddingHorizontal: 27}}>
        <Text style={styles.labelTitle}>Jelajahi Aplikasi</Text>
        <Text style={styles.txtContent}>
          Kini dengan epic mengurus surat perizinan lebih instan, buruan mulai
          sekarang!
        </Text>
        <View style={styles.buttonWrapper}>
          <Button
            theme="secondary"
            onPress={() => navigation.navigate('Login')}>
            Mulai Sekarang
          </Button>
        </View>
      </View>
      {/* <View style={styles.bottomWrapper}>
        <Text style={styles.txtContent}>
          Anda Dapat Mengajukan perizinan dibidang pendidikan dengan cepat,
          buruan mulai sekarang!
        </Text>
      </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.primary,
  },
  img: {
    height: '100%',
    width: '100%',
  },
  bottomWrapper: {
    backgroundColor: color.white,
    borderTopLeftRadius: 75,
    borderTopRightRadius: 75,
    justifyContent: 'center',
    flex: 1,
    marginTop: 70,
    paddingHorizontal: 37,
    paddingBottom: 59,
    paddingTop: 12,
  },
  labelTitle: {
    fontFamily: fonts.popinsBold,
    fontSize: 24,
    color: color.white,
    textAlign: 'center',
    marginTop: -38,
  },
  txtContent: {
    fontFamily: fonts.popinsLight,
    fontSize: 18,
    marginTop: 20,
    color: color.white,
    textAlign: 'center',
  },
  containerImage: {
    width: 258,
    height: 248,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 90,
    marginBottom: 20,
  },
  buttonWrapper: {
    // bottom: 46,
    // width: '80%',
    // alignSelf: 'center',
    marginTop: 78,
  },
});
