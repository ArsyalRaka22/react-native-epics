import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import Button from '../components/Button';
import CardList from '../components/CardList';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {fonts} from '../utils/fonts';
import ModalAlert from '../components/ModalAlert';

const dataStatus = [
  {
    status: 2,
  },
  {
    status: 2,
  },
  {
    status: 2,
  },
  {
    status: 2,
  },
  {
    status: 2,
  },
];

export default function PerizinanMasuk(props) {
  const navigation = useNavigation();
  const [search, setSearch] = useState('');
  const [isModal, setIsModal] = useState(false);

  return (
    <View style={{flex: 1, backgroundColor: color.white}}>
      <HeaderBack title="Perizinan Masuk" onPress={() => navigation.goBack()} />
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 23,
          paddingTop: 30,
          justifyContent: 'space-between',
        }}>
        <TextInputIcon
          isIcon={true}
          jenisIcons="Ionicons"
          iconName="search-outline"
          placeholder="Cari Nomor Surat"
          value={search}
          onChangeText={setSearch}
          containerStyle={styles.input}
        />

        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            setIsModal(true);
          }}
          style={{
            backgroundColor: color.primary,
            padding: 10,
            borderRadius: 10,
          }}>
          <Ionicons name="options-outline" size={24} color={color.white} />
        </TouchableOpacity>
      </View>
      <ScrollView style={{flex: 1, paddingHorizontal: 23, paddingVertical: 20}}>
        {dataStatus.map(item => {
          return (
            <>
              <CardList
                data={item}
                status={item.status}
                titleButton={'Detail Surat'}
                onPress={() => {
                  if (item.status === 1) {
                    navigation.navigate('DetailPerizinan');
                  } else {
                    setIsModal(true);
                  }
                }}
              />
            </>
          );
        })}
        <View style={{height: 26}} />

        <ModalAlert visible={isModal}>
          <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
            <Text style={styles.txtLabelModal}>Masih Tahap Perkembangan</Text>
            <Button
              theme={'secondary'}
              onPress={() => {
                setIsModal(false);
              }}>
              Kembali
            </Button>
          </View>
        </ModalAlert>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    flex: 1,
    marginRight: 10,
  },
  label: {
    fontSize: 13,
    marginBottom: 5,
  },

  txtLabelGreen: {
    color: color.labelGreen,
  },
  txtLabelRed: {
    color: color.labelRed,
  },
  txtLabelYellow: {
    color: color.labelWarning,
  },
  txtLabelTop: {
    fontSize: 15,
    fontWeight: '600',
    color: color.black,
  },
  txtTitle: {
    fontFamily: fonts.popinsBold,
    color: color.black,
    fontSize: 18,
  },
  cardStatus: {
    flex: 1,
    borderRadius: 10,
    alignSelf: 'center',
    alignContent: 'center',
  },
  cardLabel: {
    fontFamily: fonts.popinsMedium,
    paddingVertical: 11,
    paddingHorizontal: 20,
    fontSize: 15,
  },
  txtTitleCard: {
    color: '#808080',
    fontSize: 15,
    fontFamily: fonts.popinsLight,
  },
  txtContentCard: {
    fontFamily: fonts.popinsLight,
    color: color.black,
    fontSize: 15,
  },
  cardLabelBtn: {
    fontFamily: fonts.popinsMedium,
    paddingVertical: 11,
    // paddingHorizontal: 20,
    fontSize: 15,
  },
  btnForward: {
    backgroundColor: color.rgbaPrimary,
    padding: 8,
    borderRadius: 10,
    marginLeft: 10,
  },
  txtLabelModal: {
    color: color.white,
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 34,
    marginTop: 20,
    textAlign: 'center',
    alignItems: 'center',
  },
});
