import React, {useState} from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import TextInputIcon from '../components/TextInputIcon';
import Button from '../components/Button';
import color from '../utils/color';

export default function PelacakanById(props) {
  const navigation = useNavigation();
  const [idSurat, setIdSurat] = useState('');
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Pelacakan" onPress={() => navigation.goBack()} />
        <ScrollView style={styles.mainContainer}>
          <Text style={styles.label}>ID Surat</Text>
          <TextInputIcon
            isIcon={true}
            placeholder="ID Surat"
            value={idSurat}
            onChangeText={setIdSurat}
            containerStyle={styles.input}
          />
        </ScrollView>
        <View style={styles.wrapperButton}>
          <Button
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('DetailSurat');
            }}>
            Lacak Status Surat
          </Button>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },

  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 13,
    // fontFamily: fonts.montserratReguler,
    marginBottom: 5,
  },
  mainContainer: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 23,
  },
  wrapperButton: {
    backgroundColor: color.white,
    paddingVertical: 50,
    paddingHorizontal: 23,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
  },
});
