import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import NoData from '../components/NoData';
import color from '../utils/color';
import {fonts} from '../utils/fonts';
import {RadioCheckBox} from '../components/CheckBox';

const data = [
  {
    label: 'Pertanyaan Ke 1',
  },
  {
    label: 'Pertanyaan Ke 2',
  },
  {
    label: 'Pertanyaan Ke 3',
  },
];

export default function KepuasanPelanggan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Kepuasan Pelanggan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={styles.mainContainer}>
          {data.map((item, index) => {
            return (
              <View style={{flex: 1, flexDirection: 'column'}} key={index}>
                <Text style={styles.txtPertanyaan}>{item.label}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                    }}>
                    <RadioCheckBox valueRadio={''} />
                    <Text style={{fontSize: 14, fontFamily: fonts.popinsLight}}>
                      Kurang Setuju
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                    }}>
                    <RadioCheckBox valueRadio={''} />
                    <Text style={{fontSize: 14, fontFamily: fonts.popinsLight}}>
                      Setuju
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                    }}>
                    <RadioCheckBox valueRadio={''} />
                    <Text style={{fontSize: 14, fontFamily: fonts.popinsLight}}>
                      Sangat Setuju
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 27,
    marginVertical: 27,
  },
  txtPertanyaan: {
    fontSize: 18,
    fontFamily: fonts.popinsMedium,
    marginVertical: 8,
  },
});
