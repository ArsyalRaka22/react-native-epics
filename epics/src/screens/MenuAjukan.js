import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  PermissionsAndroid,
  Dimensions,
} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import Combobox from '../components/Combobox';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import Button from '../components/Button';
import Geolocation from '@react-native-community/geolocation';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

let dataJenisPerizinan = [
  {
    id: '1',
    label: 'Izin Pendirian Sekolah',
  },
  {
    id: '2',
    label: 'Daftar Ulang Izin OperasionPerizinan Pendirian',
  },
  {
    id: '3',
    label: 'Izin Operasional',
  },
  {
    id: '4',
    label: 'Perpanjangan Operasional',
  },
];
let dataKategoriPerizinan = [
  {
    id: '1',
    label: 'Paud',
  },
  {
    id: '2',
    label: 'TK',
  },
  {
    id: '3',
    label: 'SD',
  },
  {
    id: '4',
    label: 'SMP',
  },
];

const {width, height} = Dimensions.get('screen');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0035;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default function MenuAjukanPerizinan(props) {
  const navigation = useNavigation();

  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');
  const [dataMaps, setDataMaps] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Example App',
          message: 'Example App access to your location ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
        // alert('You can use the location');
      } else {
        console.log('location permission denied');
        // alert('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    requestLocationPermission();
  }, []);

  return (
    <View style={{flex: 1}}>
      <HeaderBack
        title="Ajukan Perizinan"
        onPress={() => navigation.goBack()}
      />
      <ScrollView style={styles.container}>
        <Combobox
          showSearchBar={false}
          label={'Jenis Perizinan'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataJenisPerizinan}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            // setSelectedPelanggan(val)
            console.log('val', val);
          }}
        />
        <View style={{height: 18}} />
        <Combobox
          showSearchBar={false}
          label={'Kategori Perizinan'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataKategoriPerizinan}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            // setSelectedPelanggan(val)
            console.log('val', val);
          }}
        />
        <View style={{height: 18}} />
        <Text style={styles.label}>Nama</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Nama"
          value={nama}
          onChangeText={setNama}
          containerStyle={styles.input}
        />
        <Text style={styles.label}>Alamat</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Alamat"
          value={alamat}
          onChangeText={setAlamat}
          multiline={true}
          wrapperStyle={styles.wrapperText}
          containerStyle={styles.input}
        />

        <View
          style={{
            width: 355,
            height: 185,
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <MapView
            style={{...StyleSheet.absoluteFillObject}}
            provider={PROVIDER_GOOGLE}
            initialRegion={dataMaps}
          />
        </View>
        <View style={{height: 18}} />

        <Text style={styles.label}>Longitude</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="longitude"
          value={dataMaps.longitude.toString()}
          onChangeText={setAlamat}
          containerStyle={styles.input}
        />
        <Text style={styles.label}>Latitude</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Latitude"
          value={dataMaps.latitude.toString()}
          onChangeText={setAlamat}
          containerStyle={styles.input}
        />
      </ScrollView>
      <View style={{backgroundColor: color.white, padding: 26}}>
        <Button
          onPress={() => {
            navigation.navigate('AjukanNext');
          }}>
          Lanjut
        </Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 23,
    paddingVertical: 20,
    backgroundColor: color.white,
  },
  input: {
    marginBottom: 24,
  },
  wrapperText: {
    height: 99,
    textAlign: 'left',
    alignItems: 'flex-start',
  },
  label: {
    fontSize: 13,
    // fontFamily: fonts.montserratReguler,
    marginBottom: 5,
  },
});
