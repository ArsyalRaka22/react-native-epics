import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Touchable,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {fonts} from '../utils/fonts';

const MENU_DATA = [
  {
    label: 'Semua Perizinan',
    color: color.semua_perizinan,
    img: require('../assets/dashboard/Monitoring/semua_perizinan.png'),
    count: 23,
    page: 'SemuaPerizinan',
  },
  {
    label: 'Perizinan Masuk',
    color: color.perizinan_masuk,
    img: require('../assets/dashboard/Monitoring/perizinan_masuk.png'),
    count: 23,
    page: 'PerizinanMasuk',
  },
  {
    label: 'Perizinan Terlambat',
    color: color.perizinan_terlambat,
    img: require('../assets/dashboard/Monitoring/perizinan_terlambat.png'),
    count: 23,
    page: 'PerizinanTerlambat',
  },
  {
    label: 'Perizinan Dikembalikan',
    color: color.perizinan_dikembalikan,
    img: require('../assets/dashboard/Monitoring/perizinan_dikembalikan.png'),
    count: 23,
    page: 'PerizinanDikembalikan',
  },
  {
    label: 'Perizinan Diterbitkan',
    color: color.perizinan_diterbitkan,
    img: require('../assets/dashboard/Monitoring/perizinan_diterbitkan.png'),
    count: 23,
    page: 'PerizinanDiterbitkan',
  },
];

export default function MenuMonitoring(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Monitoring" onPress={() => navigation.goBack()} />
        <ScrollView style={{paddingHorizontal: 27, paddingVertical: 26}}>
          {MENU_DATA.map((item, index) => {
            return (
              <>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => {
                    navigation.navigate(item.page);
                  }}
                  style={[styles.containerCard, {backgroundColor: item.color}]}>
                  <Image source={item.img} style={{height: 44, width: 42}} />
                  <View style={{flexDirection: 'row', flex: 1}}>
                    <View style={{flex: 1, marginLeft: 13}}>
                      <Text
                        style={{
                          fontFamily: fonts.montserratBold,
                          color: color.white,
                          fontWeight: '700',
                        }}>
                        {item.label}
                      </Text>
                      <Text
                        style={{
                          fontFamily: fonts.montserratReguler,
                          color: color.white,
                          fontWeight: '400',
                        }}>
                        {item.count} Total Perizinan
                      </Text>
                    </View>
                    <Ionicons
                      name="chevron-forward-outline"
                      color={color.white}
                      size={23}
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: 24}} />
              </>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  mainCardChat: {
    backgroundColor: 'rgba(240, 252, 255, 1)',
    flexDirection: 'row',
    paddingHorizontal: 17,
    paddingVertical: 14,
    borderRadius: 20,
    height: 96,
    alignContent: 'center',
    alignItems: 'center',
  },
  containerCard: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
    borderRadius: 10,
  },
});
