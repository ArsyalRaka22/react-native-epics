import React, {useCallback, useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Touchable,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {fonts} from '../utils/fonts';
import YoutubePlayer from 'react-native-youtube-iframe';
import UrlParser from 'js-video-url-parser';
import SimpleModal from '../components/SimpleModal';

const MENU_DATA = [
  {
    label: 'Semua Perizinan',
    color: color.semua_perizinan,
    img: require('../assets/dashboard/Monitoring/semua_perizinan.png'),
    count: 23,
  },
  {
    label: 'Perizinan Masuk',
    color: color.perizinan_masuk,
    img: require('../assets/dashboard/Monitoring/perizinan_masuk.png'),
    count: 23,
  },
  {
    label: 'Perizinan Terlambat',
    color: color.perizinan_terlambat,
    img: require('../assets/dashboard/Monitoring/perizinan_terlambat.png'),
    count: 23,
  },
  {
    label: 'Perizinan Dikembalikan',
    color: color.perizinan_dikembalikan,
    img: require('../assets/dashboard/Monitoring/perizinan_dikembalikan.png'),
    count: 23,
  },
  {
    label: 'Perizinan Diterbitkan',
    color: color.perizinan_diterbitkan,
    img: require('../assets/dashboard/Monitoring/perizinan_diterbitkan.png'),
    count: 23,
  },
];

export default function DaftarVideoTutorial(props) {
  const navigation = useNavigation();
  const [playing, setPlaying] = useState(false);
  const [linkYT, setLinkYT] = useState(
    'https://youtu.be/YsE9jJ1UOU4?si=vrDrhxkhd6DFhQQk',
  );
  const [isShowYoutube, setIsShowYoutube] = useState(false);

  const renderYt = useCallback(() => {
    let info = UrlParser.parse(linkYT);
    let id = info?.id;
    let panjangYT = linkYT.length;
    return (
      <>
        {panjangYT > 24 && (
          <>
            <View>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  setIsShowYoutube(true);
                }}
                style={{
                  borderRadius: 10,
                  paddingHorizontal: 18,
                }}>
                <Image
                  source={{
                    uri: 'https://img.youtube.com/vi/' + id + '/hqdefault.jpg',
                  }}
                  style={styles.videoImage}
                  rezizeMode="cover"
                />
                {/* <View style={{flex: 1}}> */}
                <View
                  style={{
                    backgroundColor: '#424242',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 10,
                    width: 50,
                    height: 35,
                    position: 'absolute',
                    top: '38%',
                    left: '48%',
                  }}>
                  <Ionicons name="play-outline" size={25} color={color.white} />
                </View>
                {/* </View> */}
                <View style={{height: 18}} />
              </TouchableOpacity>
            </View>
          </>
        )}

        <SimpleModal
          visible={isShowYoutube}
          onRequestClose={() => {
            setIsShowYoutube(false);
          }}
          modal={{backgroundColor: 'rgba(0, 0, 0, 0.7)'}}
          contentStyle={{
            borderColor: 'transparent',
            height: 250,
            width: 400,
            backgroundColor: 'transparent',
            borderWidth: 0,
          }}>
          <YoutubePlayer
            height={300}
            play={playing}
            videoId={id}
            onChangeState={state => {
              if (state === 'ended') {
                setPlaying(false);
                Alert.alert('video has finished playing!');
              }
            }}
          />
        </SimpleModal>
      </>
    );
  }, [isShowYoutube, linkYT, playing]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Daftar Video Panduan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView style={{paddingHorizontal: 27, paddingVertical: 26}}>
          <View style={{flexDirection: 'column'}}>
            <View style={{marginBottom: 12}}>
              <Text style={styles.txtTitle}>
                1. Panduan Cara Mengajukan Perizinan
              </Text>
            </View>
            {renderYt()}
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  mainCardChat: {
    backgroundColor: 'rgba(240, 252, 255, 1)',
    flexDirection: 'row',
    paddingHorizontal: 17,
    paddingVertical: 14,
    borderRadius: 20,
    height: 96,
    alignContent: 'center',
    alignItems: 'center',
  },
  containerCard: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
    borderRadius: 10,
  },
  txtTitle: {
    fontFamily: fonts.montserratBold,
    fontWeight: '700',
    color: color.black,
    fontSize: 15,
  },
  videoImage: {
    height: 172,
    opacity: 0.8,
  },
});
