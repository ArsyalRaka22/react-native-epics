import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';
import Combobox from '../components/Combobox';
import {fonts} from '../utils/fonts';

let dataJenisPerizinan = [
  {
    id: '1',
    label: 'Izin Pendirian Sekolah',
  },
  {
    id: '2',
    label: 'Daftar Ulang Izin OperasionPerizinan Pendirian',
  },
  {
    id: '3',
    label: 'Izin Operasional',
  },
  {
    id: '4',
    label: 'Perpanjangan Operasional',
  },
];

export default function MenuPersyaratan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Alur Persayaratan"
          onPress={() => navigation.goBack()}
        />
        <ScrollView
          style={{flex: 1, paddingHorizontal: 27, paddingVertical: 26}}>
          <Combobox
            showSearchBar={false}
            label={'Jenis Perizinan'}
            placeholder="Silahkan Pilih"
            value={''}
            theme={{
              boxStyle: {
                backgroundColor: color.white,
                paddingLeft: 30,
                borderRadius: 12,
              },
              leftIconStyle: {
                color: color.black,
                marginRight: 14,
              },
              rightIconStyle: {
                color: color.black,
              },
            }}
            data={dataJenisPerizinan}
            jenisIconsRight="Ionicons"
            iconNameRight="caret-down-outline"
            showLeftIcons={false}
            onChange={val => {
              // setSelectedPelanggan(val)
              console.log('val', val);
            }}
          />
          <View style={{height: 20}} />
          <Text
            style={[
              styles.txtLabel,
              {fontSize: 18, fontFamily: fonts.popinsBold},
            ]}>
            Alur Permohonan
          </Text>
          <Image
            source={require('../assets/AlurPermohonan.png')}
            style={{height: 195, width: '100%'}}
            resizeMode="contain"
          />
          <Text
            style={[
              styles.txtLabel,
              {fontSize: 18, fontFamily: fonts.popinsBold},
            ]}>
            Syarat Perizinan
          </Text>
          <View style={{paddingVertical: 12}}>
            <Text style={styles.txtLabel}>
              Syarat 1{' '}
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                ( Lihat Template Dokumen )
              </Text>{' '}
            </Text>
            <Text>Kurikulum tingkat satuan pendidikan</Text>
          </View>
          <View style={{paddingVertical: 12}}>
            <Text>
              Syarat 2{' '}
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                ( Lihat Template Dokumen )
              </Text>{' '}
            </Text>
            <Text>Surat Izin Mendirikan Bangunan</Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  txtLabel: {
    fontSize: 14,
    fontFamily: fonts.popinsLight,
    color: color.black,
  },
});
