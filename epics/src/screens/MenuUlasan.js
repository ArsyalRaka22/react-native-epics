import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import NoData from '../components/NoData';
import color from '../utils/color';
import {fonts} from '../utils/fonts';
import {RadioCheckBox} from '../components/CheckBox';
import moment from 'moment';

export default function MenuUlasan(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Daftar Ulasan" onPress={() => navigation.goBack()} />
        <ScrollView style={styles.mainContainer}>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              backgroundColor: '#F0FCFF',
              paddingTop: 18,
              paddingHorizontal: 23,
              paddingBottom: 27,
              borderRadius: 10,
            }}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={styles.txtTitle}>Nama Pemohon</Text>
              <Text style={styles.txtContent}>Raka Cogan</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={styles.txtTitle}>Tanggal</Text>
              <Text style={styles.txtContent}>
                {moment(new Date()).format('DD MMMM YYYY')}
              </Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 27,
    marginVertical: 27,
  },
  txtPertanyaan: {
    fontSize: 18,
    fontFamily: fonts.montserratReguler,
    fontWeight: '600',
    color: color.black,
    marginVertical: 8,
  },
  txtTitle: {
    fontSize: 15,
    fontFamily: fonts.montserratBold,
    fontWeight: '600',
    color: color.black,
    marginBottom: 6,
  },
  txtContent: {
    fontSize: 13,
    fontFamily: fonts.montserratReguler,
    fontWeight: '400',
    color: color.Neutral10,
  },
});
