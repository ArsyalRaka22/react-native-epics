import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  PermissionsAndroid,
  Dimensions,
  Image,
} from 'react-native';
import HeaderBack from '../components/HeaderBack';
import {useNavigation} from '@react-navigation/native';
import Combobox from '../components/Combobox';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import Button from '../components/Button';
import Geolocation from '@react-native-community/geolocation';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import ModalAlert from '../components/ModalAlert';

let dataJenisPerizinan = [
  {
    id: '1',
    label: 'Izin Pendirian Sekolah',
  },
  {
    id: '2',
    label: 'Daftar Ulang Izin OperasionPerizinan Pendirian',
  },
  {
    id: '3',
    label: 'Izin Operasional',
  },
  {
    id: '4',
    label: 'Perpanjangan Operasional',
  },
];
let dataKategoriPerizinan = [
  {
    id: '1',
    label: 'Paud',
  },
  {
    id: '2',
    label: 'TK',
  },
  {
    id: '3',
    label: 'SD',
  },
  {
    id: '4',
    label: 'SMP',
  },
];

const {width, height} = Dimensions.get('screen');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0035;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default function AjukanNext(props) {
  const navigation = useNavigation();

  const [nama, setNama] = useState('');
  const [alamat, setAlamat] = useState('');
  const [isModal, setIsModal] = useState('');

  return (
    <View style={{flex: 1}}>
      <HeaderBack
        title="Ajukan Perizinan"
        onPress={() => navigation.goBack()}
      />
      <ScrollView style={styles.container}>
        <Text style={styles.label}>Kurikulum tingkat satuan pendidikan</Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TextInputIcon
            isIcon={false}
            value={nama}
            onChangeText={setNama}
            isSecureTextEntry={false}
            jenisIconRight="Ionicons"
            iconNameRight="cloud-upload-outline"
            containerStyle={styles.input}
          />
          <Image
            source={require('../assets/download.png')}
            style={{height: 51, width: 51, marginLeft: 22}}
          />
        </View>

        <Text style={styles.label}>Surat Izin Mendirikan Bangunan</Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TextInputIcon
            isIcon={false}
            value={nama}
            onChangeText={setNama}
            isSecureTextEntry={false}
            jenisIconRight="Ionicons"
            iconNameRight="cloud-upload-outline"
            containerStyle={styles.input}
          />
          <Image
            source={require('../assets/download.png')}
            style={{height: 51, width: 51, marginLeft: 22}}
          />
        </View>
      </ScrollView>
      <View style={{backgroundColor: color.white, padding: 26}}>
        <Button
          onPress={() => {
            setIsModal(true);
          }}>
          Lanjut
        </Button>
      </View>
      <ModalAlert visible={isModal}>
        <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
          <Text style={styles.txtLabelModal}>Perizinan Berhasil Diajukan</Text>
          <Button
            theme={'secondary'}
            onPress={() => {
              setIsModal(false);
            }}>
            Cetak Nota
          </Button>
          <View style={{height: 26}} />
          <Button
            theme={'reverse-primary'}
            onPress={() => {
              setIsModal(false);
              navigation.navigate('Dashboard');
            }}>
            Kembali
          </Button>
        </View>
      </ModalAlert>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 23,
    paddingVertical: 20,
    backgroundColor: color.white,
  },
  input: {
    marginBottom: 24,
    flex: 1,
  },
  wrapperText: {
    height: 99,
    textAlign: 'left',
    alignItems: 'flex-start',
  },
  label: {
    fontSize: 13,
    // fontFamily: fonts.montserratReguler,
    marginBottom: 5,
  },
  txtLabelModal: {
    color: color.white,
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 34,
    marginTop: 20,
    textAlign: 'center',
    alignItems: 'center',
  },
});
