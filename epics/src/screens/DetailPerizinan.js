import React, {useCallback, useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import color from '../utils/color';
import Button from '../components/Button';
import HeaderBack from '../components/HeaderBack';
import {useSelector} from 'react-redux';
import {fonts} from '../utils/fonts';
import ModalAlert from '../components/ModalAlert';

export default function DetailPerizinan(props) {
  const navigation = useNavigation();
  const user = useSelector(state => state.user);
  const [isModal, setIsModal] = useState(false);

  const renderPemohon = useCallback(() => {
    return (
      <>
        <ScrollView style={{paddingHorizontal: 23, paddingVertical: 23}}>
          <View
            style={{
              backgroundColor: color.white,
              paddingHorizontal: 18,
              paddingVertical: 18,
              borderRadius: 20,
              borderColor: color.primary,
              borderWidth: 1,
            }}>
            {/* <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Jenis Perizinan</Text>
              <Text>N/A</Text>
            </View> */}
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jenis Perizinan</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>No Surat</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Tanggal</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Status</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Nama Instansi</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jadwal Survey</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Longitude</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Latitude</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Alamat</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>
                Kurikulum tingkat satuan pendidikan
              </Text>
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                Lihat Dokumen
              </Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Surat Izin Mendirikan Bangunan</Text>
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                Lihat Dokumen
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={{backgroundColor: color.white, padding: 26}}>
          <Button
            activeOpacity={0.7}
            onPress={() => {
              // navigation.navigate('ChatDetail');
              setIsModal(true);
            }}>
            Chat Petugas
          </Button>
        </View>
      </>
    );
  }, []);
  const renderOperator = useCallback(() => {
    return (
      <>
        <ScrollView style={{paddingHorizontal: 23, paddingVertical: 23}}>
          <View
            style={{
              backgroundColor: color.white,
              paddingHorizontal: 18,
              paddingVertical: 18,
              borderRadius: 20,
              borderColor: color.primary,
              borderWidth: 1,
            }}>
            {/* <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Jenis Perizinan</Text>
              <Text>N/A</Text>
            </View> */}
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jenis Perizinan</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>No Surat</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Tanggal</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Status</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Nama Instansi</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Jadwal Survey</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginBottom: 6}}>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Longitude</Text>
                <Text>N/A</Text>
              </View>
              <View style={{flex: 1, marginBottom: 6}}>
                <Text style={styles.Label}>Latitude</Text>
                <Text>N/A</Text>
              </View>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Alamat</Text>
              <Text>N/A</Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>
                Kurikulum tingkat satuan pendidikan
              </Text>
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                Lihat Dokumen
              </Text>
            </View>
            <View style={{flex: 1, marginBottom: 6}}>
              <Text style={styles.Label}>Surat Izin Mendirikan Bangunan</Text>
              <Text
                style={{
                  color: color.primary,
                  textDecorationLine: 'underline',
                  fontFamily: fonts.popinsLight,
                }}>
                Lihat Dokumen
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={{backgroundColor: color.white, padding: 26}}>
          <Button
            theme="secondary"
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('KembalikanPerizinan');
              // setIsModal(true);
            }}>
            Kembalikan
          </Button>
          <View style={{height: 30}} />
          <Button
            activeOpacity={0.7}
            onPress={() => {
              // navigation.navigate('ChatDetail');
              setIsModal(true);
            }}>
            Validasi
          </Button>
        </View>
      </>
    );
  }, [navigation]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack
          title="Detail Perizinan"
          onPress={() => navigation.goBack()}
        />
        {user.role === 'Pemohon' && renderPemohon()}
        {user.role === 'Operator' && renderOperator()}
        <ModalAlert visible={isModal}>
          <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
            <Text style={styles.txtLabelModal}>Masih Tahap Perkembangan</Text>
            <Button
              theme={'secondary'}
              onPress={() => {
                setIsModal(false);
              }}>
              Kembali
            </Button>
          </View>
        </ModalAlert>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  Label: {
    fontSize: 15,
    fontFamily: fonts.montserratBold,
    fontWeight: '600',
    color: color.black,
  },
  txtPrimaryColor: {
    fontSize: 15,
    color: color.primary,
    fontWeight: '600',
  },
  txtLabelModal: {
    color: color.white,
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 34,
    marginTop: 20,
    textAlign: 'center',
    alignItems: 'center',
  },
});
