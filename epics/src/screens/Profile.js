import React from 'react';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  ImageBackground,
  Dimensions,
} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import color from '../utils/color';
import Button from '../components/Button';
import {useDispatch, useSelector} from 'react-redux';
import {setUser} from '../store/actions';
import HeaderTablet from '../components/HeaderTablet';
import app from '../config/app';
import HeaderProfile from '../components/HeaderProfile';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

export default function Profile(props) {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const user = useSelector(state => state.user);

  let data = [
    {
      label: 'Role',
      item: user.role,
    },
    {
      label: 'Nama',
      item: '',
    },
    {
      label: 'Email',
      item: '',
    },
    {
      label: 'Nomor Telepon',
      item: '',
    },
    {
      label: 'Tanggal Lahir',
      item: '',
    },
    {
      label: 'Jenis Kelamin',
      item: '',
    },
    {
      label: 'Jenis Identitas',
      item: '',
    },
    {
      label: 'Nomor Identitas',
      item: '',
    },
    {
      label: 'Provinsi',
      item: '',
    },
    {
      label: 'Kota',
      item: '',
    },
    {
      label: 'Kecamatan',
      item: '',
    },
    {
      label: 'Kelurahan',
      item: '',
    },
    {
      label: 'Pekerjaan',
      item: '',
    },
  ];

  let dataOperator = [
    {
      label: 'Nama Lengkap',
      item: '',
    },
    {
      label: 'User',
      item: '',
    },
    {
      label: 'Role',
      item: user.role,
    },
  ];

  return (
    <>
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.mainContainer}
          showsVerticalScrollIndicator={false}>
          <HeaderProfile
            textHeader={app.NAME}
            textProfile={styles.txtProfile}
            textAlamat={styles.txtProfileContent}
            // iconProfile={() => {
            //     navigation.navigate("Profile")
            // }}
            iconRight={() => {
              navigation.navigate('Notification');
            }}
          />
          <View style={styles.containerProfile}>
            <View style={{paddingHorizontal: 27, paddingVertical: 28}}>
              {/* {data.map((item, index) => {
                return (
                  <View key={index} style={styles.containerTxt}>
                    <Text style={styles.txtLabel}>{item.label}</Text>
                    <View style={{flex: 1}} />
                    <Text style={styles.txtLabel}>{item.item}</Text>
                  </View>
                );
              })} */}
              {user.role !== 'Pemohon' && (
                <>
                  {dataOperator.map((item, index) => {
                    return (
                      <View key={index} style={styles.containerTxt}>
                        <Text style={styles.txtLabel}>{item.label}</Text>
                        <View style={{flex: 1}} />
                        <Text style={styles.txtLabel}>{item.item}</Text>
                      </View>
                    );
                  })}
                </>
              )}
              {user.role !== 'Operator' && user.role === 'Pemohon' && (
                <>
                  {data.map((item, index) => {
                    return (
                      <View key={index} style={styles.containerTxt}>
                        <Text style={styles.txtLabel}>{item.label}</Text>
                        <View style={{flex: 1}} />
                        <Text style={styles.txtLabel}>{item.item}</Text>
                      </View>
                    );
                  })}
                </>
              )}
              <View style={{height: 36}} />
              <Text>
                Nb: Untuk Informasi lebih lengkap atau memperbaiki informasi
                silahkan akses website Epic
              </Text>
              <View style={styles.gapBottom} />
              <Button
                theme="faq"
                jenisIcons="MaterialCommunityIcons"
                iconName="comment-question-outline"
                colorIcon={color.white}
                iconSize={15}
                textStyle={{fontSize: 14, marginLeft: 9}}
                loading={false}
                onPress={() => {
                  navigation.navigate('Faq');
                }}>
                FAQs
              </Button>
              <View style={{height: 36}} />
              <Button
                theme="logout"
                jenisIcons="Ionicons"
                iconName="exit-outline"
                colorIcon={color.white}
                iconSize={15}
                textStyle={{fontSize: 14, marginLeft: 9}}
                loading={false}
                onPress={() => {
                  dispatch(setUser(null));
                }}>
                Keluar
              </Button>
            </View>
          </View>
        </ScrollView>

        <BottomTab
          selected={4}
          onClick={event => {
            navigation.navigate(event);
          }}
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerProfile: {
    top: -60,
    marginHorizontal: 27,
    marginVertical: 27,
    backgroundColor: color.white,
    borderRadius: 20,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  containerImage: {
    width: 115,
    height: 115,
    alignSelf: 'center',
    marginVertical: 47,
  },
  mainContainer: {
    flex: 1,
  },
  gapBottom: {
    height: 29,
  },
  containerTxt: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  txtLabel: {
    fontSize: 16,
    fontWeight: '600',
    color: color.black,
  },
});
