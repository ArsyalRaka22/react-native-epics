import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Alert,
  StatusBar,
} from 'react-native';
import {useDispatch} from 'react-redux';
import Button from '../components/Button';
import color from '../utils/color';
import app from '../config/app';
import TextInputIcon from '../components/TextInputIcon';
import {fonts} from '../utils/fonts';
import {setUser} from '../store/actions';
import {ScrollView} from 'react-native-gesture-handler';
import Combobox from '../components/Combobox';
import HeaderTransparent from '../components/HeaderTransparent';
import ModalAlert from '../components/ModalAlert';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';

const dataRole = [
  {
    id: 'Pemohon',
    label: 'Pemohon',
  },
  {
    id: 'Operator',
    label: 'Operator',
  },
  {
    id: 'Verifikator',
    label: 'Verifikator',
  },
  {
    id: 'AdminDinas',
    label: 'Admin Dinas',
  },
  {
    id: 'Surveyor',
    label: 'Surveyor',
  },
  {
    id: 'KepalaDinas',
    label: 'Kepala Dinas',
  },
  {
    id: 'Bupati',
    label: 'Bupati / Walikota',
  },
  {
    id: 'Auditor',
    label: 'Auditor',
  },
  {
    id: 'AdminUtama',
    label: 'Admin Utama',
  },
];

export default function Login({navigation}) {
  const dispatch = useDispatch();
  const [isLoading, setLoading] = useState(false);
  const [email, setEmail] = useState(__DEV__ ? app.EXAMPLE_EMAIL : '');
  const [password, setPassword] = useState(__DEV__ ? app.EXAMPLE_PASSWORD : '');
  const [jenisRole, setJenisRole] = useState('');
  const [isModal, setIsModal] = useState(false);

  // const login = useCallback(() => {
  //     setLoading(true);
  //     let data = { username, password };
  //     HttpRequest.login(data).then((res) => {
  //         console.log("Res", res.data);
  //         Toast.showSuccess("Login Success");
  //         setLoading(false);
  //         dispatch(setUser(res.data));
  //     }).catch((err) => {
  //         console.log(err, err.response);
  //         Toast.showError(err.response.data.message);
  //         setLoading(false);
  //     });
  // }, [username, password]);

  const toggleLogin = useCallback(() => {
    if (jenisRole === '') {
      Alert.alert('Informasi', 'Jenis Role harap dipilih');
    } else {
      dispatch(setUser({role: jenisRole}));
    }
  }, [dispatch, jenisRole]);

  const toggleGoogleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const token = await GoogleSignin.getTokens();

      console.log('userInfo', userInfo);
      console.log('token', token);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('ini error');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('ini progress');
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('ini not avaible');
        // play services not available or outdated
      } else {
        console.log('ini error lainnya some other', error.code);
        // some other error happened
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderTransparent />
      <ScrollView style={styles.content}>
        <View style={{alignSelf: 'center', marginBottom: 31, marginTop: 10}}>
          <Text style={styles.headerTitle}>Masuk Epic</Text>
        </View>
        {/* <Text style={styles.titleName}>{app.NAME}</Text>
        <Text style={[styles.titleName, {fontSize: 18, alignSelf: 'center'}]}>
          Masuk
        </Text> */}

        <Combobox
          showSearchBar={false}
          label={'Jenis Role'}
          placeholder="Silahkan Pilih"
          value={''}
          theme={{
            boxStyle: {
              backgroundColor: color.white,
              paddingLeft: 30,
              borderRadius: 12,
            },
            leftIconStyle: {
              color: color.black,
              marginRight: 14,
            },
            rightIconStyle: {
              color: color.black,
            },
          }}
          data={dataRole}
          jenisIconsRight="Ionicons"
          iconNameRight="caret-down-outline"
          showLeftIcons={false}
          onChange={val => {
            setJenisRole(val);
          }}
        />
        <View style={{marginBottom: 6}} />

        <Text style={styles.label}>Email</Text>
        <TextInputIcon
          isIcon={true}
          placeholder="Username"
          value={email}
          onChangeText={setEmail}
          containerStyle={styles.input}
        />

        <Text style={styles.label}>Kata Sandi</Text>

        <TextInputIcon
          isIcon={true}
          placeholder="Kata Sandi"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={true}
          containerStyle={styles.input}
        />

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('LupaKataSandi');
          }}>
          <Text style={styles.txtLupaSandi}>Lupa Kata Sandi</Text>
        </TouchableOpacity>
      </ScrollView>

      <View style={styles.wrapperButton}>
        <Button
          loading={isLoading}
          onPress={() => {
            toggleLogin();
          }}>
          Masuk
        </Button>
        <View style={{height: 29}} />
        <TouchableOpacity
          style={styles.btnGoogle}
          activeOpacity={0.7}
          onPress={() => {
            // setIsModal(true);
            toggleGoogleSignIn();
          }}>
          <Image
            source={require('../assets/google.png')}
            style={{height: 29, width: 29, marginRight: 4}}
            resizeMode="cover"
          />
          <Text
            style={{
              fontFamily: fonts.popinsLight,
              color: color.black,
              fontSize: 18,
            }}>
            Masuk Dengan Google
          </Text>
        </TouchableOpacity>
        <ModalAlert visible={isModal}>
          <View style={{paddingHorizontal: 34, paddingVertical: 33}}>
            <Text style={styles.txtLabelModal}>Masih Tahap Perkembangan</Text>
            <Button
              theme={'secondary'}
              onPress={() => {
                setIsModal(false);
              }}>
              Kembali
            </Button>
          </View>
        </ModalAlert>
        {/* <View
          style={[
            styles.containerText,
            {marginBottom: 90, marginTop: 48, alignSelf: 'center'},
          ]}>
          <Text style={styles.txtLabel}>Belum punya akun? </Text>

          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              navigation.navigate('Register');
            }}>
            <Text style={styles.txtLabelPrimary}>Daftar Sekarang</Text>
          </TouchableOpacity>
        </View> */}
      </View>
      {/* <View
        style={{
          flexDirection: 'row',
          position: 'absolute',
          bottom: 20,
          alignSelf: 'center',
        }}>
        <Text style={styles.txtLabel}>Belum punya akun? </Text>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          <Text style={styles.txtLabelPrimary}>Daftar Sekarang</Text>
        </TouchableOpacity>
      </View> */}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
  content: {
    paddingHorizontal: 20,
  },
  titleName: {
    fontSize: 20,
    fontFamily: fonts.popinsLight,
    marginBottom: 20,
  },
  input: {
    marginBottom: 24,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 5,
    color: color.black,
  },

  bottomWrap: {
    flex: 4,
    paddingHorizontal: 20,
  },

  lineWrap: {
    paddingVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#333333',
  },
  textBetween: {
    color: color.white,
    marginHorizontal: 10,
    fontSize: 14,
  },

  buttonNoBg: {
    backgroundColor: color.black,
    // marginBottom: 30,
    marginHorizontal: 20,
    borderWidth: 0,
  },
  signInText: {
    fontWeight: 'bold',
    color: color.primary,
  },

  imageWallpaper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },

  footerForm: {
    flexDirection: 'row',
    marginBottom: 26,
    alignItems: 'center',
  },
  checkBox: {
    width: 24,
    height: 24,
  },
  txtForgotPassword: {
    fontSize: 16,
    textAlign: 'right',
    color: color.primary,
    fontFamily: fonts.popinsLight,
  },
  txtRememberMe: {
    fontSize: 16,
    flex: 1,
    marginLeft: 8,
    color: color.Neutral80,
    fontFamily: fonts.montserratReguler,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  txtLabel: {
    fontSize: 16,
  },
  txtLabelPrimary: {
    color: color.primary,
    fontSize: 16,
    marginLeft: 5,
    fontFamily: fonts.popinsLight,
  },
  containerText: {
    marginTop: 18,
    flexDirection: 'row',
    alignItems: 'center',
  },
  wrapperButton: {
    flexDirection: 'column',
    paddingHorizontal: 37,
    marginBottom: 80,
  },
  headerTitle: {
    fontFamily: fonts.popinsMedium,
    fontSize: 32,
    color: color.black,
  },
  txtLupaSandi: {
    fontFamily: fonts.popinsMedium,
    fontSize: 16,
    color: color.black,
    textAlign: 'right',
  },

  txtLabelModal: {
    color: color.white,
    fontSize: 18,
    fontFamily: fonts.popinsLight,
    marginBottom: 34,
    marginTop: 20,
    textAlign: 'center',
    alignItems: 'center',
  },
  btnGoogle: {
    flexDirection: 'row',
    height: 60,
    borderRadius: 60,
    borderColor: color.primary,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
