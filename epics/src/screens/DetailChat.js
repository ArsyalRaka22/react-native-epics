import React from 'react';
import {SafeAreaView, Text, View, ScrollView, StyleSheet} from 'react-native';
import BottomTab from '../components/BottomTab';
import {useNavigation} from '@react-navigation/native';
import HeaderTitle from '../components/HeaderTitle';
import HeaderBack from '../components/HeaderBack';
import color from '../utils/color';
import TextInputIcon from '../components/TextInputIcon';
import TextInputChat from '../components/TextInputChat';

export default function DetailChat(props) {
  const navigation = useNavigation();
  return (
    <>
      <SafeAreaView style={styles.container}>
        <HeaderBack title="Live Chat" onPress={() => navigation.goBack()} />
        <ScrollView>{/* <Text>ini adalah Chat</Text> */}</ScrollView>
        <View style={{paddingHorizontal: 27, marginBottom: 20}}>
          <TextInputChat
            isIcon={true}
            jenisIconsRight="Ionicons"
            iconNameRight="send-outline"
            colorIcon={color.white}
            wrapperStyle={{height: 60}}
          />
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.white,
  },
});
