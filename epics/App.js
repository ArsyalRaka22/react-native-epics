import React, {useEffect, useState} from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import {Provider as ReduxProvider} from 'react-redux';

import {store} from './src/store';
import NavigatorProvider from './src/navigator/mainNavigator';
import FlashMessage from 'react-native-flash-message';
import {persistStore} from 'redux-persist';
import {LogLevel, OneSignal} from 'react-native-onesignal';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

OneSignal.initialize('78f36c43-942f-46e4-a155-aab7ecfdf1cc');
console.disableYellowBox = true;

export default function App(props) {
  const [ready, setReady] = useState(false);

  const loadInitialData = async () => {
    await new Promise(resolve => {
      persistStore(store, null, async () => {
        resolve(null);
      });
    });

    await OneSignal.Notifications.requestPermission(true);
    console.log('Permission granted for notifications');

    setReady(true);
  };

  useEffect(() => {
    loadInitialData();
  }, []);

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '182614155699-jq3pjc8eb4tn633lp3g0lbkm7np92a45.apps.googleusercontent.com', // client ID of type WEB for your server. Required to get the idToken on the user object, and for offline access.
      offlineAccess: true,
    });
  }, []);

  if (ready) {
    return (
      <ReduxProvider store={store}>
        <StatusBar barStyle="light-content" />
        <NavigatorProvider />
        <FlashMessage position="top" />
      </ReduxProvider>
    );
  } else {
    return null;
  }
}
